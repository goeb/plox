<?xml version="1.0" encoding="UTF-8"?>

<!--

This is an example Plox flavour template for (X)HTML output, you may use it as a basis for your own templates. It
includes all important data to show on the blog page, for some of it plugins are required (they are included in this
distribution), see the comments below.

-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>

   <!-- The BetterTitle plugin must be enabled for this: -->

   <title>[% BetterTitle %]</title>

   <!-- The atom plugin must be enabled for Atom feeds. Include the main feed: -->

   <link rel="alternate" type="application/atom+xml" title="» main" href="/index.atom" />

   <!-- If we're not in the top level category, include this category's feed: -->

   [% IF path.category != '/' %]
      <link
         rel="alternate"
         type="application/atom+xml"
         title="» main » [% path.categories.join(' » ')  %]"
         href="[% path.category %]index.atom"
      />
   [% END %]

</head>

<body>

   <!-- Loop through the different dates for which stories are to be shown on the requested page: -->

   [% FOREACH day IN stories.order %]

   <!-- Link the date in the header, for date-based filtering: -->

      <h1>
         <a href="/[% day.y %]/index.html" title="Filter by year">[% day.y %]</a> /
         <a href="/[% day.y %]/[% day.m %]/index.html" title="Filter by month">[% day.m %]</a> /
         <a href="/[% day.y %]/[% day.m %]/[% day.d %]/index.html" title="Filter by day">[% day.d %]</a>
      </h1>

      <!-- Now, for the current date, loop through the stories of that date: -->
   
      [% FOREACH story IN stories.${day.date} %]

         <!-- Provide a permanent link with the story title: -->

         <h2><a href="[% story.basename %].html" title="Permanent link">[% story.title %]</a></h2>

         <p>

            <!-- Story time, show hour:minutes: -->

            Posted at [% story.time.0 %]:[% story.time.1 %]

            <!-- If an author header exists, show that: -->

            [% IF story.headers.exists ('author') AND story.headers.author %]
               by [% story.headers.author %]
            [% END %]

         </p>

         <!-- This includes the actual content of the story -->

         [% story.text %]

         <!-- Gallery stuff - the Gallery plugin is still experimental! -->

         [% IF story.Gallery AND path.type != 'story' %]
            [% FOREACH image IN story.Gallery.images %]
               <div>
                  <a href="/gallery[% story.basename %]/[% image.main.filename %]">
                     <img
                        src="/gallery[% story.basename %]/[% image.thumbnail %]"
                        alt="[% story.Gallery.description.${image.main.filename} %]"
                     />
                  </a>
               </div>
            [% END %]
         [% ELSIF story.Gallery %]
            [% FOREACH image IN story.Gallery.images %]
               <div>
                  <a href="/gallery[% story.basename %]/[% image.main.filename %]">
                     <img
                        src="/gallery[% story.basename %]/[% image.thumbnail %]"
                        alt="[% story.Gallery.description.${image.main.filename} %]"
                     />
                  </a>
                  <ul>
                     <li>
                        [% image.main.width %]x[% image.main.height %] —
                        [% image.main.filesize / 1024 / 1024 FILTER format('%.1f') %] MiB
                     </li>
                     <li>
                        posted: [% image.main.year %]/[% image.main.month %]/[% image.main.day %]
                     </li>
                     <li>
                        [% image.main.format %]
                     </li>
                     [% IF image.alternatives > 0 %]
                        <li>
                           other formats:
                           [% FOREACH alt IN image.alt_images %]
                              <a href="/gallery[% story.basename %]/[% alt.filename %]">[[% loop.count %]]</a>
                           [% END %]
                        </li>
                     [% END %]
                  </ul>
                  [% IF story.Gallery.description.${image.main.filename} %]
                     [% story.Gallery.description.${image.main.filename} %]
                  [% ELSE %]
                     (no description available)
                  [% END %]
               </div>
            [% END %]
         [% END %]

         <!-- If the SeyMour plugin is enabled and the current story is not completely shown: -->

         [% IF story.SeyMour AND path.type != 'story' %]
            <p><a href="[% story.basename %].html" title="Permanent link">Read more...</a></p>
         [% END %]

         <p>

            Posted in category:

            [% FOREACH category IN story.categories %]

               <!-- Display links to all parent categories of the story: -->

               » <a href="[% category.path %]index.html" title="Category: [% category.name %]">[% category.name %]</a>

            [% END %]

         </p>

      [% END %]

   [% END %]

   <!-- End main (dates) loop -->

   <!-- If the MoreEntries plugin is enabled, show the next entries/previous entries links: -->

   [% IF MoreEntries.defined %]

      <p>

         [% IF MoreEntries.next %]

            <!-- There is a next page (ie. previous entries), show the link: -->

            <a href="[% path.path %]page-[% MoreEntries.next %]/index.html">previous entries</a>

         [% ELSE %]

            <!-- No next page: -->

            no previous entries

         [% END %]

      </p>

      <p>

         [% IF MoreEntries.prev AND MoreEntries.prev != 1 %]

            <a href="[% path.path %]page-[% MoreEntries.prev %]/index.html">next entries</a>

         [% ELSIF MoreEntries.prev %]

            <!-- We do not include the page number in the URL for the first page: -->

            <a href="[% path.path %]index.html">next entries</a>

         [% ELSE %]

            no next entries

         [% END %]

      </p>

   [% END %]

   <!-- End MoreEntries -->

   <p>

      <!-- This information is provided by the ProcessTime plugin: -->

      Time until template processing starts: [% ProcessTime %]s<br />
      Template processing: <!--ProcessTimeTT-->s<br />
      Complete page generation: <!--ProcessTimeAll-->s

   </p>

</body>

</html>

<!-- :indentSize=3:tabSize=3:noTabs=true:mode=html:maxLineLen=120: -->