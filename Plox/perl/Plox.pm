# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox;

use warnings;
use strict;
use utf8;

use CGI;
use Plox::Config;
use Plox::Plugin;

our $VERSION = '0.1';

=head1 NAME

Plox - The main module for the Plox blogging system

=head1 SYNOPSIS

   use Plox;

   my $plox = new Plox ();

   my ($return, $data) = $plox -> generate ();

   if ($return & Plox::ERROR) {
      # do something to log the error
   }

   unless ($return & Plox::NO_OUTPUT) {
      # print the headers in $data -> {'headers'}:
      while (my ($header, $value) = each %{$data -> {'headers'}}) {
         print $header, ': ', $value, "\n";
      }
      print "\n";
      # and print the page content in $data -> {'output'}:
      print $data -> {'output'};
   }

=head1 DESCRIPTION

This is the main module of I<Plox>, the one (and only) module you need to access from the CGI script to get I<Plox>
running. There are only a few methods that are useful for the end user, these are described below.

=head1 REQUIREMENTS

This module requires the L<CGI> module to be available.

=head1 CONSTANTS

This module defines the constants C<OK>, C<ERROR>, C<NO_OUTPUT> and C<TERMINATE>, see the descriptions of the methods
below for details on how and when they are used.

=cut

use constant {
   'OK'        => 0,
   'ERROR'     => 1,
   'NO_OUTPUT' => 2,
   'TERMINATE' => 4,
};

=head1 METHODS

=head2 new

This method creates (and returns) a new I<Plox> instance:

   my $plox = new Plox ();

There are a number of parameters you can specify, in hash form:

   my $plox = new Plox ('key1' => 'value1', 'key2' => 'value2' ...);

These parameters are:

=over

=item C<'cgi'>

This may be set to a valid existing L<CGI> instance to be used by the I<Plox::*> modules. If it is not set, a new L<CGI>
instance will be created. Useful for FastCGI applications, see the included file F<plox.fcgi> for an example.

=item C<'template'>

A L<Template> object. As with the C<'cgi'> parameter, this will be created if not supplied. See F<plox.fcgi> for an
example.

=item C<'cache'>

A general cache variable, may be used by plugins to keep some data in memory during requests. Again, this is only useful
with FastCGI scripts, see F<plox.fcgi> for an example.

=back

I<new()> will setup some initial data, read the configuration and load all plugins (except those configured not to be
loaded). If either of these actions fails, I<new()> will return an undefined value. At least one plugin must be loaded
to succeed.

=cut

sub new {
   my ($module, %args) = (@_);
   my $self = bless {}, $module;
   $self -> {'terminate'} = 0;
   $self -> {'data'} {'template'} = $args {'template'};
   $self -> {'data'} {'cache'}    = $args {'cache'};
   $self -> {'data'} {'cgi'}      = $args {'cgi'} || new CGI ();
   $self -> {'data'} {'cgi'} -> charset ('UTF-8');
   return unless $self -> _configure () and $self -> _plugins_load ();
   return $self;
}

=head2 generate

This method does the actual work of creating the content of the requested page. It requires no arguments and returns an
array of two elements:

   my ($return, $data) = $plox -> generate ();

=over

=item C<$return>

This is the return value, a bitmask using the constants mentioned above.

If the C<ERROR> bit is set, an error occured during the processing, i.e. one of the plugins' hooks returned an error.
There is no way to check which plugin caused the error. This is purely informational, the main application may use the
C<ERROR> returned to log it or inform the user somehow, but processing should continue as normal.

If the C<NO_OUTPUT> bit is set, the main application must not print the generated content, including the HTTP headers.
In this case, the main application must assume that the plugin that caused this bit to be set already sent a complete
page to the client.

C<TERMINATE> is set (always together with C<NO_OUTPUT>) if the L<terminate()|/terminate> method has been run, typically
by the main application.

Note that C<OK> shouldn't be used directly, it is defined as C<0>, i.e. no bit set (this may change in the future).

=item C<$data>

This is a hashref, similar to the C<$data> hashref the plugins use (see L<Plox::Plugin> for details). However, it does
not contain all the elements used during page generation. The following keys are provided in this hashref (the variable
name is the key name, the variable type indicates the type, this format is used throughout the documentation):

=over

=item C<$output>

The complete content of the generated page. The main application should send this to the client (after the headers),
unless C<$return> has the C<NO_OUTPUT> bit set.

=item C<%headers>

This is a hashref itself, containing the HTTP response headers (and special headers meant for the server to indicate
the response code). The header names are the keys, the contents of the headers are the values in this hashref. Again,
the main application should sent the headers to the client - before the actual page content - unless the C<NO_OUTPUT>
bit is set.

=item C<$template>

The L<Template> instance used during page generation. For FastCGI scripts this may be cached and passed to the
L<new()|/new> method on the next request. This avoids creating a new L<Template> object on every request.

=item C<$cgi>

The L<CGI> instance used during processing the current request. Useful only if you need to perform some tasks with a CGI
instance afterwards to avoid creating a new one, or to cache it like the template instance.

=item C<%cache>

A general cache hashref, which should be passed to the new Plox instance on the next request created in a FastCGI
script. This may be used by plugins to cache whatever they like.

=back

Note that though it is guaranteed that the elements exist in C<$data>, it is not guaranteed that either of it is defined
or not empty (though these would be errors in most cases unless indicated by C<$return>), the main application should
check these before trying to print the content.

=back

The I<generate()> method just runs all plugins' L<main hooks|Plox::Plugin/HOOKS> in the configured order, sets the
return code according to the return values of the hooks and eventually returns C<$data> as described above. The hook
return codes - and thus the behaviour of this method - are documented in L<Plox::Plugin>, as well as the defined hooks
and their purpose.

This method will check the terminate flag (set if the L<terminate()|/terminate> method is called) before every hook is
run and return C<(NO_OUTPUT | TERMINATE, undef)> in case the flag is set.

=cut

use constant 'HOOKS'   => qw( path_info entries_root entries sort filter stories pre_process process post_process );
use constant 'S_HOOKS' => qw( accept_file entry filter_entry story                                                );

sub generate {
   my ($self) = @_;
   foreach (S_HOOKS) {
      $self -> {'data'} {'hooks'} {$_} = $self -> _plugins_sort ($_, $self -> {'data'} {'plugins'});
   }
   my $return = 0;
   HOOK: foreach my $hook (HOOKS) {
      my $plugins = $self -> _plugins_sort ($hook, $self -> {'data'} {'plugins'});
      foreach my $plugin (@$plugins) {
         return (NO_OUTPUT | TERMINATE, undef) if $self -> {'terminate'};
         my $result = $self -> {'data'} {'plugins'} {$plugin} -> $hook ($self -> {'data'});
         delete $self -> {'data'} {'plugins'} {$plugin} if $result & Plox::Plugin::UNLOAD;
         $return |= ERROR if $result & Plox::Plugin::ERROR;
         if ($result & Plox::Plugin::LAST) {
            $return |= NO_OUTPUT if $result & Plox::Plugin::NO_OUTPUT;
            last HOOK;
         }
         last if $result & Plox::Plugin::NEXT_HOOK;
      }
   }
   return (
      $return,
      {
         'output'    => $self -> {'data'} {'output'  },
         'headers'   => $self -> {'data'} {'headers' },
         'template'  => $self -> {'data'} {'template'},
         'cgi'       => $self -> {'data'} {'cgi'     },
         'cache'     => $self -> {'data'} {'cache'   },
      }
   );
}

=head2 terminate

This method asks the I<Plox> instance to stop processing the current request:

   $plox -> terminate ();

If this method is called, a terminate flag is set which is checked regularly during the L<generate()|/generate> method,
see above for details on how this is handled. Processing is not stopped immediately! This method always returns C<1>.

=cut

sub terminate {
   my ($self) = @_;
   return $self -> {'terminate'} = 1;
}

=head1 PRIVATE METHODS

The following information is only useful for developers:

=head2 _configure

Sets up C<< $self -> {'data'} {'conf'} >> using the L<Plox::Config::config()|Plox::Config/config> method, see
L<Plox::Config>. Returns a true value on success, else a false value. L<Plox::Config::config()|Plox::Config/config> must
return a hashref containing all the configuration settings.

=cut

sub _configure {
   my ($self) = @_;
   return ($self -> {'data'} {'conf'} = Plox::Config -> config ()) ? 1 : undef;
}

=head2 _plugins_load

Looks for I<Plox::Plugin::*> plugin modules in all paths in C<@INC> and C<use>s them. Plugins can be excluded and loaded
in a specific order, see L<Plox::Config> for details. If a plugin could not be loaded due to an error, the function
returns a false value. Also, if no plugin was found or no plugin was loaded due to the configuration, a false value is
returned.

=over

B<Note:> A plugin may refuse to be loaded by simply returning a false value from its L<new()|Plox::Plugin/new> method,
this is not considered an error and the function will continue with the next plugin (if any).

=back

If everything went fine, I<_plugins_load()> returns true. The plugin instances will be stored in the hashref
C<< $self -> {'data'} {'plugins'} >>, with the plugin base name (i.e. file name without path and without extension)
being the key.

=over

B<Note:> Only absolute paths in C<@INC> (i.e. paths starting with a slash) are considered when looking for plugin
modules!

=back

=cut

sub _plugins_load {
   my ($self) = @_;
   my %plugins;
   foreach my $dir (@INC) {
      next unless substr ($dir, 0, 1) eq '/';
      while (my $module = glob "$dir/Plox/Plugin/*.pm") {
         $module =~ s{.*/([a-zA-Z0-9_-]+)\.pm}{$1};
         next if $self -> {'data'} {'conf'} {'plugins_skip'} {$module};
         $plugins {$module} = 1;
      }
   }
   foreach (keys %plugins) {
      eval "use Plox::Plugin::$_";
      return if $@;
   }
   my $sorted = $self -> _plugins_sort ('new', \%plugins);
   foreach (@$sorted) {
      eval "\$self -> {'data'} {'plugins'} {\$_} = Plox::Plugin::$_ -> new (\$self -> {'data'})";
      return if $@;
      delete $self -> {'data'} {'plugins'} {$_} unless $self -> {'data'} {'plugins'} {$_};
   }
   return $self -> {'data'} {'plugins'} and %{$self -> {'data'} {'plugins'}};
}

=head2 _plugins_sort

This method returns an arrayref containing the names of plugins, sorted according to the configuration for a specific
hook. It requires two arguments: C<$hook> and C<$plugins>:

   my $sorted = $self -> _plugins_sort ($hook, $plugins);

C<$hook> is the name of the hook for which a sorted list should be returned. The list will only contain the name of
plugins that actually implement the specific hook. C<$plugins> must be a hashref, the keys of this must be the names of
all plugins to consider, the values the plugin instances. Alphabetical order is used for plugins if not otherwise
configured.

See L<Plox::Config> on details how to configure the order of plugins' hooks.

The arrayref returned by this function may be empty.

B<Note:> Existence of a plugin in C<$plugins> is generally not tested, so make sure there are only valid plugin names in
this arrayref. The existence of plugins specified to run first or last in the configuration is checked.

=cut

sub _plugins_sort {
   my ($self, $hook, $plugins) = @_;
   my %last;
   if ($self -> {'data'} {'conf'} {'hooks_last'} {$hook}) {
      @last {@{$self -> {'data'} {'conf'} {'hooks_last'} {$hook}}} = ();
   }
   my (%done, @sorted);
   if ($self -> {'data'} {'conf'} {'hooks_first'} {$hook}) {
      foreach (@{$self -> {'data'} {'conf'} {'hooks_first'} {$hook}}) {
         next if not $plugins -> {$_} or $done {$_} or not "Plox::Plugin::$_" -> can ($hook);
         push @sorted, $_;
         $done {$_} = 1;
      }
   }
   my @plugins = sort keys %$plugins;
   foreach (@plugins) {
      next if exists $last {$_} or $done {$_} or not "Plox::Plugin::$_" -> can ($hook);
      push @sorted, $_;
      $done {$_} = 1;
   }
   if (%last) {
      foreach (@{$self -> {'data'} {'conf'} {'hooks_last'} {$hook}}) {
         next if not $plugins -> {$_} or $done {$_} or not "Plox::Plugin::$_" -> can ($hook);
         push @sorted, $_;
      }
   }
   return \@sorted;
}

1;

=head1 INTERNALS

This module is merely a plugin loader, it does some basic initialization and configuration tasks and loads all plugins
available (unless configured otherwise) and then runs all regular hooks in the specified order (see documentation in
L<Plox::Plugin>). All actual work related to the gathering of page content is left up to the plugins, most of it is done
by the core plugin L<Plox::Plugin::Plox>.

There are a few special hooks which are not directly run by this module. These are called by the core module from within
other (regular) hook functions. Again, see L<Plox::Plugin> for details.

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: