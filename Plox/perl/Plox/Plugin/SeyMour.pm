# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::SeyMour;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1.2';

=head1 NAME

Plox::Plugin::SeyMour - Show only the first part of an entry on index pages

=head1 DESCRIPTION

This plugin allows you to put a separator string in your story. On index pages, all text after (and including) this
separator will be deleted, so only the first part of your story is shown. On story pages, the separator will be replaced
by an anchor (with the ID C<'seymour'>), so you can directly link to the part of the story not shown on index pages, the
remaining text is kept intact (to not create this anchor, set a C<seymour-no-anchor> header in your story to C<1>).

=head1 CONFIGURATION

=over

=item C<$cut>

The string used to separate a story.

=back

=head1 METHODS

The following hooks are implemented:

=head2 story

L<General information for this hook.|Plox::Plugin/story>

This hook will modify the story text, depending on the type of the requested page. For C<'story'> pages, the configured
C<$cut> string will be replaced by an anchor with the ID C<'seymour'>, nothing else will be modified in this case. For
all other page types, the configured string and every text following it will be deleted.

If a modification was made by this hook, the per-story variable C<< $story -> {'SeyMour'} >> will be set to C<1>, else
it will be C<0>.

This hook must be executed after the story text was read from the files, i.e. after the
L<core plugin's story|Plox::Plugin::Plox/story> hook.

=cut

sub story {
   my ($self, $data, $entry, $story) = @_;
   return (Plox::Plugin::REFUSED, $story) unless $story;
   $story -> {'SeyMour'} = 0;
   if ($data -> {'path'} {'type'} eq 'story') {
      if (not $story -> {'headers'} {'seymour-no-anchor'}) {
         $story -> {'SeyMour'} = 1 if $story -> {'text'} =~ s!\Q$self->{cut}\E!<a id="seymour"></a>!;
      }
      else {
         $story -> {'SeyMour'} = 1 if $story -> {'text'} =~ /\Q$self->{cut}\E/;
      }
   }
   else {
      $story -> {'SeyMour'} = 1 if $story -> {'text'} =~ s/\Q$self->{cut}\E.*//s;
   }
   return (Plox::Plugin::OK, $story);
}

1;

=head1 CHANGELOG

=over

=item 2009/07/07

=over

=item *

version 0.1.2

=item *

minor update: introduced handling of C<seymour-no-anchor> story header

=back

=item 2009/06/08

=over

=item *

version 0.1.1

=item *

bug fixed: uninitialized or undefined C<$story> not treated correctly

=back

=item 2009/05/29

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: