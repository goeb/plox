# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::Plox;

use warnings;
use strict;
use utf8;
use open ':encoding(UTF-8)';

use base 'Plox::Plugin';

use Encode;
use Fcntl qw (:DEFAULT :flock);
use File::Find;
use Template;

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin::Plox - Plox core plugin

=head1 DESCRIPTION

This is the main plugin of I<Plox>, it does most of the work to generate the requested page. Without this plugin,
I<Plox> won't be useful at all, so do not disable it (unless you want to replace it with another plugin providing the
same functionality)!

=head1 CONFIGURATION

The following options must be set in L<Plox::Config>, the variable name must be the name of the key in the
C<< $config -> {'Plox'} >> hashref, see L<Config.pm|Plox::Config> for examples:

=over

=item C<$default_page>

The page name for which index pages are generated. This name cannot be used as a story name. If the client requests this
page or does not request a specific page at all (usually every URL ending with a C<'/'>) an index page will be sent.

=item C<$default_flavour>

If the client did not specify a (pseudo-)filename with the requested URL, this flavour will be used to generate the
output.

=item C<$data_dir>

Top level directory of the story files. It must be an absolute path (with leading C<'/'>) and it must not end with a
C<'/'>.

=item C<$extension>

Filename extension of the story files, including the leading C<'.'>.

=item C<$max_depth>

Maximum directory (category) depth on index pages. If this is set to C<0>, only the stories of the requested category
are included on index pages, subcategories are ignored. If set to any other positive number it specifies the maximum
depth of subcategories for which stories are included. A value of C<-1> means unlimited.

=item C<$future_entries>

Set to a true value to include stories with a modification time in the future, set to a false value to ignore these.

=item C<$max_entries>

This specifies the maximum number of stories included on index pages. A value of C<0> means no limit.

=item C<$flavour_dir>

The directory where the templates are located (absolute path). Without trailing C<'/'>.

=item C<$compile_dir> and C<$compile_ext>

Set these values to the directory where the compiled templates should be saved and to the extension that should be used
for these files. These values are passed to L<Template Toolkit|Template> (as C<COMPILE_EXT> and C<COMPILE_DIR> options
when the L<Template> instance is created), see L<Template> documentation for details. If you do not want to cache
compiled templates, set both options to C<undef>.

=item C<%types>

A hash(ref) used to assign content types to the different flavours. The hash's keys must be the flavour names, the
values must be the content type specification to send to the client in the HTTP header.

=back

=head1 REQUIREMENTS

The L<Template> module (i.e. L<Template Toolkit|Template>) is required.

=cut

=head1 METHODS

The methods described below are the hooks described in L<Plox::Plugin>, please look there for a general description of
what each hook is supposed to do. Implementation details are documented below.

=head2 path_info

L<General information for this hook.|Plox::Plugin/path_info>

Provides the path information in C<< $data -> {'path'} >>. It will do nothing if C<< $data -> {'path'} {'page'} >> and
C<< $data -> {'path'} {'flavour'} >> are already set, C<Plox::Plugin::REFUSED> is returned in that case. If the user
does not explicitely request a specific page (and flavour) these two variables will be set to the default values as
specified in the configuration.

The requested path is split into page, categories and date if applicable. Note that detection of dates is very loose,
invalid dates are easily possible. Note also that date detection prohibits a top level category with a name that looks
like a valid year. Categories, page names and flavours must only include C<[a-zA-Z0-9-_]> characters, everything else is
considered invalid and is ignored. Page name and flavour must be separated by a dot (C<'.'>).

The page type will be set in C<< $data -> {'path'} {'type'} >>, it is either C<'main'> (top level index page, e.g.
for L<http://example.org/>), C<'category'> for index pages of a specific category, C<'date'> for index pages for a
specific date, C<'both'> for index pages of categories restricted to a specific date or C<'story'> if a single story was
requested.

If a date is requested (i.e. either year, year and month or year, month and day) C<< $data -> {'page'} {'date'} >> will
be set to an array(ref) containing the year, month and day, in that order. If an element is not set in the requested URL
it will be set to an empty string in this array. If the URL does not specify a date at all, all three elements will be
empty strings.

After this hook, the following stuff is available in C<$data>:

=over

=item C<< $data -> {'path'} {'page'} >>

Requested page (or default value).

=item C<< $data -> {'path'} {'flavour'} >>

Requested flavour (or default value).

=item C<< $data -> {'path'} {'type'} >>

Page type (see explanation above).

=item C<< $data -> {'path'} {'categories'} >>

Arrayref containing the requested categories, may be empty.

=item C<< $data -> {'path'} {'category'} >>

Categories, joined with C<'/'>.

=item C<< $data -> {'path'} {'date'} >>

Date arrayref, as described above.

=item C<< $data -> {'path'} {'invalid'} >>

Invalid path components.

=item C<< $data -> {'path'} {'path'} >>

Date and categories, joined with C<'/'>, without page name.

=back

=cut

sub path_info {
   my ($self, $data) = @_;
   return Plox::Plugin::REFUSED if $data -> {'path'} {'page'} and $data -> {'path'} {'flavour'};
   $data -> {'path'} {'page'   } = $self -> {'default_page'   };
   $data -> {'path'} {'flavour'} = $self -> {'default_flavour'};
   my @paths = split m{/}, decode_utf8 ($data -> {'cgi'} -> path_info ());
   my (@category, @date, @invalid);
   while (@paths) {
      my $path = shift @paths;
      if (
         not @category and (
            ($path =~ /^(?:19|20)\d\d$/     and $#date == -1) or
            ($path =~ /^(?:0\d|1[012])$/    and $#date ==  0) or
            ($path =~ /^(?:[012]\d|3[01])$/ and $#date ==  1)
         )
      ) {
         push @date, $path;
      }
      elsif ($path =~ /^[a-zA-Z0-9_-]+$/) {
         push @category, $path;
      }
      elsif ($path =~ /^([a-zA-Z0-9_-]+)\.([a-zA-Z0-9_-]+)$/ and not @paths) {
         $data -> {'path'} {'page'   } = $1;
         $data -> {'path'} {'flavour'} = $2;
      }
      else {
         push @invalid, $path if $path;
      }
   }
   if ($data -> {'path'} {'page'} eq $self -> {'default_page'}) {
      if    ( @category and @date ) { $data -> {'path'} {'type'} = 'both';     }
      elsif ( @category           ) { $data -> {'path'} {'type'} = 'category'; }
      elsif ( @date               ) { $data -> {'path'} {'type'} = 'date';     }
      else                          { $data -> {'path'} {'type'} = 'main';     }
   }
   else                             { $data -> {'path'} {'type'} = 'story';    }
   map { $date [$_] = '' unless $date [$_] } (0 .. 2);
   $data -> {'path'} {'categories'} = \@category;
   $data -> {'path'} {'category'  } = $self -> _join_path (@category);
   $data -> {'path'} {'date'      } = \@date;
   $data -> {'path'} {'invalid'   } = \@invalid;
   $data -> {'path'} {'path'      } = $self -> _join_path (@date, @category);
   return Plox::Plugin::OK;
}

=head2 entries_root

L<General information for this hook.|Plox::Plugin/entries_root>

Sets up the base directory to look for story files. The two entries in C<$data> this hook creates are:

=over

=item C<< $data -> {'entries'} {'base'} >>

Basically the same as the C<$data_dir> configuration option.

=item C<< $data -> {'entries'} {'category'} >>

C<$data_dir> plus the requested category.

=back

Both values will start with a C<'/'> and end with a C<'/'>. The hook does nothing and returns C<Plox::Plugin::REFUSED>
if these two values are already set.

=cut

sub entries_root {
   my ($self, $data) = @_;
   return Plox::Plugin::REFUSED if $data -> {'entries'} {'category'} and $data -> {'entries'} {'base'};
   $data -> {'entries'} {'base'}     = $self -> _join_path ($self -> {'data_dir'});
   $data -> {'entries'} {'category'} = $self -> _join_path (
      $self -> {'data_dir'}, @{$data -> {'path'} {'categories'}}
   );
   return Plox::Plugin::OK;
}

=head2 entries

L<General information for this hook.|Plox::Plugin/entries>

Collects all potential story files for a page. This hook will run the two special hooks
L<accept_file|Plox::Plugin/accept_file> and L<entry|Plox::Plugin/entry> for all files found (on story pages only the
L<entry|Plox::Plugin/entry> hook is run for the specific file). Files below the configured maximum directory depth are
not considered for inclusion. See the description for the two special hooks below for more details.

After this hook, the following data will be available in C<$data> for every file found (and accepted):

=over

=item C<< $data -> {'entries'} {'files'} {<base name>} {'mtime'} >>

The C<< <base name> >> will be the path of the file relative to the C<$data_dir> configuration option, without any
leading slash, and including the file extension. The value of C<< $data -> ... {'mtime'} >> will be used to sort the
files in the L<sort|/sort> hook later on, and will be set to the modification time of the file (see the L<entry|/entry>
hook for details).

=back

If C<< $data -> {'entries'} {'files'} >> is already set (i.e. a true value) the hook will do nothing.

=cut

sub entries {
   my ($self, $data) = @_;
   return Plox::Plugin::REFUSED if not $data -> {'entries'} {'category'} or $data -> {'entries'} {'files'};
   if ($data -> {'path'} {'type'} eq 'story') {
      $self -> _run_entry_hooks ($data, "$data->{entries}{category}$data->{path}{page}$self->{extension}");
   }
   else {
      my $root_depth = $data -> {'entries'} {'category'} =~ tr{/}{};
      my $wanted = sub {
         my $depth = $File::Find::name =~ tr{/}{};
         return if $self -> {'max_depth'} != -1 and $depth > $root_depth + $self -> {'max_depth'};
         my $accept;
         foreach my $plugin (@{$data -> {'hooks'} {'accept_file'}}) {
            next unless $data -> {'plugins'} {$plugin};
            $accept = $data -> {'plugins'} {$plugin} -> accept_file ($data, $File::Find::name);
            last if $accept & (Plox::Plugin::YES | Plox::Plugin::NEXT_ENTRY);
         }
         return unless $accept & Plox::Plugin::YES;
         $self -> _run_entry_hooks ($data, $File::Find::name);
      };
      no warnings 'File::Find';
      find ({'wanted' => $wanted, 'no_chdir' => 1}, $data -> {'entries'} {'category'});
   }
   return Plox::Plugin::OK;
}

sub _run_entry_hooks {
   my ($self, $data, $file) = @_;
   my $entry;
   foreach my $plugin (@{$data -> {'hooks'} {'entry'}}) {
      next unless $data -> {'plugins'} {$plugin};
      (my $result, $entry) = $data -> {'plugins'} {$plugin} -> entry ($data, $file, $entry);
      last if $result & Plox::Plugin::NEXT_ENTRY;
   }
   if ($entry and %$entry) {
      $file =~ s!^\Q$data->{entries}{base}\E/*!!;
      $data -> {'entries'} {'files'} {$file} = $entry;
   }
   return;
}

=head2 accept_file

L<General information for this hook.|Plox::Plugin/accept_file>

Decides if a file should be included in a page's output. This is the case if the file's basename (including possible
category directories) consists entirely of C<[a-zA-Z0-9_-]> characters and if the file extension matches the configured
extension.

=cut

sub accept_file {
   my ($self, $data, $file) = @_;
   my $root      = $data -> {'entries'} {'category'};
   my $extension = $self -> {'extension'};
   return Plox::Plugin::YES if $file =~ m{\Q$root\E(?:[a-zA-Z0-9_-]+/)*([a-zA-Z0-9_-]+)\Q$extension\E$};
   return Plox::Plugin::OK;
}

=head2 entry

L<General information for this hook.|Plox::Plugin/entry>

If the C<$entry> parameter is unset (or an empty hashref), this hook will provide the modification time of a story file
that is later used for sorting the stories. If C<$entry> is set (and not an empty hashref) this hook will assume that
another plugin provided the necessary data already and do nothing.

The return values of this hook are described in L<Plox::Plugin>, the C<$entry> hashref will provide the file
modification time in C<< $entry -> {'mtime'} >>.

Since this plugin needs to I<stat()> the file to get the modification time, it will use the cached results of the call
to check if the file is a file (i.e. no directory etc.) and if it is readable, if one of these tests fails the hook will
return C<(Plox::Plugin::REFUSED, $entry)> (with C<$entry> being the initial value).

=cut

sub entry {
   my ($self, $data, $file, $entry) = @_;
   unless ($entry and %$entry) {
      my $mtime = (stat $file) [9];
      return (Plox::Plugin::OK, {'mtime' => $mtime}) if $mtime and -f _ and -r _;
   }
   return (Plox::Plugin::REFUSED, $entry);
}

=head2 sort

L<General information for this hook.|Plox::Plugin/sort>

If there are any files entries in C<< $data -> {'entries'} {'files'} >>, this hook will sort them according to their
modification time, which must be stored in C<< $data -> {'entries'} {'files'} {<base name>} {'mtime'} >> (see above).
They will be sorted in descending order (i.e. newest file first), if two entries have the same modification time, these
will be sorted aphabetically (case independent).

The hook will create the following entries in C<$data>:

=over

=item C<< $data -> {'entries'} {'order'} >>

This will be an array(ref), containing the entries in the correct order. The values in this array will be the base
names of the entries, as used in C<< $data -> {'entries'} {'files'} {<base name>} >>.

=back

The hook will return C<Plox::Plugin::REFUSED> if there were no entries to sort - or if the entries are already sorted,
i.e. C<< $data -> {'entries'} {'order'} >> exists and is not empty, else C<Plox::Plugin::OK>.

=cut

sub sort {
   my ($self, $data) = @_;
   return Plox::Plugin::REFUSED if $data -> {'entries'} {'order'} and @{$data -> {'entries'} {'order'}};
   my @order;
   if ($data -> {'entries'} {'files'} and %{$data -> {'entries'} {'files'}}) {
      my $sort = sub {
         my $x = $data -> {'entries'} {'files'} {$a} {'mtime'};
         my $y = $data -> {'entries'} {'files'} {$b} {'mtime'};
         return $x == $y ? lc ($a) cmp lc ($b) : $y <=> $x;
      };
      push @order, $_ foreach (sort $sort keys %{$data -> {'entries'} {'files'}});
   }
   $data -> {'entries'} {'order'} = \@order;
   return @order ? Plox::Plugin::OK : Plox::Plugin::REFUSED;
}

=head2 filter

L<General information for this hook.|Plox::Plugin/filter>

This hook will filter out unwanted entries. It does this by running the L<filter_entry|Plox::Plugin/filter_entry> hooks
for any entry, and additionally checking for the maximum number of entries per page.

This hook modifies the following variables in C<$data>:

=over

=item *

C<< $data -> {'entries'} {'order'} >>

=item *

C<< $data -> {'entries'} {'files'} >>

=back

The meaning of these variables will not change (see above), but both lists will be modified to include only entries that
passed the L<filter_entry|Plox::Plugin/filter_entry> hooks, the order of entries will be retained.

Additionally, the following variable may be set:

=over

=item C<< $data -> {'entries'} {'more'} >>

It will be set to a true value if there were more entries to be kept than the maximum number of entries per page (as
specified by the C<$max_entries> configuration option). Note that this key will not exist if there are not more entries!

=back

The hook returns C<Plox::Plugin::REFUSED> if C<< $data -> {'entries'} {'order'} >> is empty, else it returns
C<Plox::Plugin::OK>.

=cut

sub filter {
   my ($self, $data) = @_;
   return Plox::Plugin::REFUSED unless @{$data -> {'entries'} {'order'}};
   $self -> {'time'} = time;
   my @filtered;
   foreach my $entry (@{$data -> {'entries'} {'order'}}) {
      unless ($data -> {'entries'} {'more'}) {
         my $keep = 1;
         foreach my $plugin (@{$data -> {'hooks'} {'filter_entry'}}) {
            next unless $data -> {'plugins'} {$plugin};
            my $result;
            ($result, $keep) = $data -> {'plugins'} {$plugin} -> filter_entry ($data, $entry, $keep, \@filtered);
            last if $result & Plox::Plugin::NEXT_ENTRY;
         }
         if ($keep) {
            if ($self -> {'max_entries'} and scalar @filtered >= $self -> {'max_entries'}) {
               $data -> {'entries'} {'more'} = 1;
            }
            else {
               push @filtered, $entry;
               next;
            }
         }
      }
      delete $data -> {'entries'} {'files'} {$entry};
   }
   $data -> {'entries'} {'order'} = \@filtered;
   return Plox::Plugin::OK;
}

=head2 filter_entry

L<General information for this hook.|Plox::Plugin/filter_entry>

The I<filter_entry> hook of this plugin checks only the entry's time (i.e. the modification time of the file) and
discards the entry if its time is either in the future (and the C<$future_entries> configuration option is false) or the
date of the entry does not match the date specified by the requested URL on pages of the type C<'date'> or C<'both'>.

The returned list's first element is always C<Plox::Plugin::OK>, the second either true or false depending on the date
checks.

=cut

sub filter_entry {
   my ($self, $data, $entry, $keep, $filtered) = @_;
   unless ($self -> {'future_entries'}) {
      if ($data -> {'entries'} {'files'} {$entry} {'mtime'} > $self -> {'time'}) {
         return (Plox::Plugin::OK, 0);
      }
   }
   if ($data -> {'path'} {'type'} eq 'date' or $data -> {'path'} {'type'} eq 'both') {
      my ($sec, $min, $hour, $d, $m, $y, $dow, $doy, $dst) = $self -> _local_time (
         $data -> {'entries'} {'files'} {$entry} {'mtime'}
      );
      my ($py, $pm, $pd) = @{$data -> {'path'} {'date'}};
      if (not $pm and not $pd) {
         return (Plox::Plugin::OK, 0) unless $y == $py;
      }
      elsif (not $pd) {
         return (Plox::Plugin::OK, 0) unless $y == $py and $m == $pm;
      }
      else {
         return (Plox::Plugin::OK, 0) unless $y == $py and $m == $pm and $d == $pd;
      }
   }
   return (Plox::Plugin::OK, 1);
}

=head2 stories

L<General information for this hook.|Plox::Plugin/stories>

This hook will run the special hook L<story|Plox::Plugin/story> for every remaining entry in
C<< $data -> {'entries'} {'order'} >>. The special hook must provide the complete data for an entry, see below for the
variables the core plugin uses to return the story. If for an entry no valid story data is returned, this entry will be
discarded (i.e. it will be deleted from the C<< $data -> {'entries'} {'order'} >> arrayref and from the
C<< $data -> {'entries'} {'files'} >> hashref).

The following additions to the C<$data> hashref are provided by this hook:

=over

=item C<< $data -> {'stories'} {'order'} >>

An arrayref, containing the dates of stories found. This allowes grouping the stories by date in the final page output,
see the example template for details. The array will contain hashrefs with the following keys:

=over

=item *

C<'date'> - date in the format C<'YYYY/MM/DD'>

=item *

C<'y'> - the year

=item *

C<'m'> - the month (leading zero if required)

=item *

C<'d'> - the day (leading zero if required)

=item *

C<'dow'> - the day of week (0 = Sunday)

=item *

C<'doy'> - the day of year

=back

=item C<< $data -> {'stories'} {<date>} >>

These are arrayrefs, containing all stories, separated by date and sorted by modification time, newest first. The date
will be in the format C<'YYYY/MM/DD'>, the elements of the arrays are story hashrefs, with the story data as described
below.

=back

The hook will return C<Plox::Plugin::REFUSED> if there are no entries in C<< $data -> {'entries'} {'order'} >>, else
C<Plox::Plugin::OK>.

=cut

sub stories {
   my ($self, $data) = @_;
   return Plox::Plugin::REFUSED unless @{$data -> {'entries'} {'order'}};
   $data -> {'stories'} {'order'} = [];
   my (@order, %dates);
   foreach my $entry (@{$data -> {'entries'} {'order'}}) {
      my $story;
      foreach my $plugin (@{$data -> {'hooks'} {'story'}}) {
         next unless $data -> {'plugins'} {$plugin};
         my $result;
         ($result, $story) = $data -> {'plugins'} {$plugin} -> story ($data, $entry, $story);
         last if $result & Plox::Plugin::NEXT_ENTRY;
      }
      if ($story and %$story) {
         push @order, $entry;
         my ($y, $m, $d) = @{$story -> {'date'}};
         push @{$data -> {'stories'} {"$y/$m/$d"}}, $story;
         unless ($dates {"$y/$m/$d"}) {
            push @{$data -> {'stories'} {'order'}}, {
               'date' => "$y/$m/$d",
               'y'    => $y,
               'm'    => $m,
               'd'    => $d,
               'dow'  => $story -> {'time_info'} [0],
               'doy'  => $story -> {'time_info'} [1],
            };
            $dates {"$y/$m/$d"} = 1;
         }
         next;
      }
      delete $data -> {'entries'} {'files'} {$entry};
   }
   $data -> {'entries'} {'order'} = \@order;
   return Plox::Plugin::OK;
}

=head2 story

L<General information for this hook.|Plox::Plugin/story>

This special hook provides the story data for an entry. If the third parameter C<$story> is already set to a non-empty
hashref, it will do nothing and return C<(Plox::Plugin::REFUSED, $story)>. The story file must be readable, I<flock()>
is used for file locking. The format of a story file is described in the F<README> file.

If there is an error when opening, locking or closing the file, the hook will return C<(Plox::Plugin::ERROR, undef)>. If
the first line of the file is empty (i.e. no story title), C<(Plox::Plugin::REFUSED, undef)> is returned.

The hook will provide the following data in the C<$story> variable that is returned if there was no error:

=over

=item C<< $story -> {'date'} >>

Arrayref, contains year, month, day in that order, with leading zeroes.

=item C<< $story -> {'time'} >>

Arrayref, contains hours, minutes, seconds in that order, with leading zeroes.

=item C<< $story -> {'time_info'} >>

Arrayref, contains day of week, day of year, daylight savings time flag.

=item C<< $story -> {'headers'} >>

Hashref, contains the story's headers (key = name, value = value).

=item C<< $story -> {'title'} >>

Title of the story.

=item C<< $story -> {'text'} >>

Story text.

=item C<< $story -> {'categories'} >>

Arrayref, see L<_categories()|Plox::Plugin/categories> documentation in L<Plox::Plugin>.

=item C<< $story -> {'basename'} >>

Base name of the story, with leading slash, without file extension.

=back

Note that some of these variables are required by the I<story> hook, L<see above|/story>.

=cut

sub story {
   my ($self, $data, $entry, $story) = @_;
   return (Plox::Plugin::REFUSED, $story) if $story and %$story;
   my $file = $data -> {'entries'} {'base'} . $entry;
   return (Plox::Plugin::ERROR, undef) unless open (STORY, '<', $file) and flock (STORY, LOCK_SH);
   my ($text, $title, %headers) = ('', scalar <STORY>, ());
   return (Plox::Plugin::REFUSED, undef) unless $title;
   chomp ($title);
   while (my $line = <STORY>) {
      chomp $line;
      if ($line =~ /^\s*([a-zA-Z0-9_-]+):\s+(.*)$/) {
         $headers {$1} = $headers {$1} ? $headers {$1} . "\n" . $2 : $2;
         next;
      }
      $text = "$line\n" if $line;
      last;
   }
   while (my $line = <STORY>) {
      chomp $line;
      $text .= "$line\n";
   }
   close STORY or return (Plox::Plugin::ERROR, undef);
   $text =~ s/^\s*|\s*$//g;
   (my $base = $entry) =~ s/\Q$self->{extension}\E$//;
   my ($sec, $min, $hour, $d, $m, $y, $dow, $doy, $dst) = $self -> _local_time (
      $data -> {'entries'} {'files'} {$entry} {'mtime'}
   );
   $story = {
      'date'       => [ $y,    $m,   $d   ],
      'time'       => [ $hour, $min, $sec ],
      'time_info'  => [ $dow,  $doy, $dst ],
      'headers'    => \%headers,
      'title'      => $title,
      'text'       => $text,
      'categories' => $self -> _categories ($entry),
      'basename'   => "/$base",
   };
   return (Plox::Plugin::OK, $story);
}

=head2 process

L<General information for this hook.|Plox::Plugin/process>

This hook uses L<Template Toolkit|Template> to generate the final page output. Other plugins may set the following
options to modify the bahaviour:

=over

=item C<< $data -> {'tt_settings'} {'path'} >>

If set, used instead of the C<$flavour_dir> setting.

=item C<< $data -> {'tt_settings'} {'settings'} >>

Arguments for L<Template>'s I<new()> method.

=item C<< $data -> {'tt_settings'} {'file'} >>

Template file to use instead of flavour template.

=item C<< $data -> {'tt_settings'} {'force_new'} >>

Force creating a new L<Template> instance.

=back

None of these are required, default options will be used if these are not set. In case you set the C<'settings'>
variable, you must include the C<'INCLUDE_PATH'> option (usually the C<$flavour_dir>) by yourself.

If there is an existing template instance in C<< $data -> {'template'} >> no new one will be created unless the
C<< $data -> {'tt_settings'} {'force_new'} >> variable is set to a true value.

The hook will set the following variables in C<$data>:

=over

=item C<< $data -> {'output'} >>

Page content as returned by Template Toolkit.

=item C<< $data -> {'headers'} >>

Page headers, like content type.

=back

If there was an error the output will be set to an error message, and the headers are set appropriately.

=cut

sub process {
   my ($self, $data) = @_;
   my $template;
   if (not $data -> {'template'} or $data -> {'tt_settings'} {'force_new'}) {
      $data -> {'tt_settings'} {'path'}     ||= $self -> {'flavour_dir'};
      $data -> {'tt_settings'} {'settings'} ||= {
         'INCLUDE_PATH' => $data -> {'tt_settings'} {'path'},
         'INTERPOLATE'  => 0,
         'DEFAULT'      => 'default.flavour',
         'COMPILE_DIR'  => $self -> {'compile_dir'},
         'COMPILE_EXT'  => $self -> {'compile_ext'},
         'PRE_CHOMP'    => 2,
         'POST_CHOMP'   => 2,
         'ENCODING'     => 'utf8',
      };
      $template = Template -> new ($data -> {'tt_settings'} {'settings'});
   }
   else {
      $template = $data -> {'template'};
   }
   $data -> {'output'} = '';
   $data -> {'tt_settings'} {'file'} ||= $data -> {'path'} {'flavour'} . '.flavour';
   unless ($template -> process ($data -> {'tt_settings'} {'file'}, $data, \$data -> {'output'})) {
      $data -> {'headers'} = {
         'Status'       => '500',
         'Content-type' => 'text/plain; charset=utf-8',
      };
      ($data -> {'output'} = <<"      END") =~ s/^\s{9}//gm;
         Internal Server Error ― It is not your fault… Or is it?

         The error was:

            ⚠  Could not process the template

         This may be a temporary problem, please try again later.
      END
      return Plox::Plugin::ERROR;
   }
   $data -> {'headers'} {'Content-type'} = $self -> {'types'} {$data -> {'path'} {'flavour'}};
   $data -> {'template'} = $template;
   return Plox::Plugin::OK;
}

1;

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: