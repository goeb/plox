# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::XhtmlMime;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin::XhtmlMime - Plugin to change the MIME type to XHTML if the client supports it

=head1 DESCRIPTION

This plugin will change the content type to the value of the C<$type> option (which should be something like
C<'application/xhtml+xml; charset=utf-8'>) if the page flavour matches an entry in the C<@flavours> option and the
client accepts XHTML pages with a higher priority than HTML pages (as specified by the client's HTTP_ACCEPT header).

The plugin also works with the L<NotFound plugin|Plox::Plugin::NotFound>, changing the content type of 404 error pages.

The content type for regular pages (and - if enabled - for 404 pages) should be set to a C<'text/html'> one, so that the
XHTML content type is only used when this plugin changes it. Note that your templates must be valid XHTML pages to be
displayed correctly, and they should be compatible to HTML.

=head1 CONFIGURATION

=over

=item C<@flavours>

The MIME type will be changed for the flavours included in this array.

=item C<$type>

Specifies the new MIME type.

=item C<$404>

If set to true, the MIME type of 404 pages will be changed. Requires the L<NotFound plugin|Plox::Plugin::NotFound> to
be enabled.

=back

=head1 METHODS

The following hooks are implemented:

=head2 new

L<General information for this hook.|Plox::Plugin/new>

The plugin will only be loaded if the HTTP_ACCEPT header indicates that the client prefers XHTML pages over HTML pages.

=cut

sub new {
   my ($module, $data) = @_;
   return undef if
      not $ENV {'HTTP_ACCEPT'} or
      $ENV {'HTTP_ACCEPT'} !~ m{application/xhtml\+xml} or
      $data -> {'cgi'} -> Accept ('text/html') < $data -> {'cgi'} -> Accept ('application/xhtml+xml');
   my $self = $module -> SUPER::new ($data);
   return bless $self, $module;
}

=head2 post_process

L<General information for this hook.|Plox::Plugin/post_process>

Changes the content type if required. Note that this hook must be run after the
L<NotFound plugin's post_process|Plox::Plugin::NotFound/post_process> hook if the
L<NotFound plugin|Plox::Plugin::NotFound> is enabled.

=cut

sub post_process {
   my ($self, $data) = @_;
   my %flavours;
   @flavours {@{$self -> {'flavours'}}} = ();
   if (exists $flavours {$data -> {'path'} {'flavour'}} or ($data -> {'NotFound'} {'404'} and $self -> {'404'})) {
      $data -> {'headers'} {'Content-type'} = $self -> {'type'};
   }
   return Plox::Plugin::OK;
}

1;

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: