# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::NotFound;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1.1';

=head1 NAME

Plox::Plugin::NotFound - Generate 404 error pages

=head1 DESCRIPTION

This plugin enables 404 (File Not Found) error messages if a requested page does not contain any stories or if an
invalid category was requested. You need to create a custom template for the error page (do not use the standard flavour
extension for this file!). Additionally, a 404 error can be sent if a flavour does not exist.

This plugin must be one of the last plugins processing the I<pre_process> and I<post_process> hooks. If the
L<XhtmlMime plugin|Plox::Plugin::XhtmlMime> is enabled, the I<post_process> hook of the I<NotFound> plugin must be run
before that of the L<XhtmlMime plugin|Plox::Plugin::XhtmlMime>.

If another plugin sets a C<Status> header before this plugin runs the I<post_process> hook, it will not change it!

=head1 CONFIGURATION

=over

=item C<$check_flavour>

If true, I<NotFound> will check if the requested flavour exists.

=item C<$template>

Template for the error page, relative to the C<$flavour_dir> configuration option of the
L<core plugin|Plox::Plugin::Plox>.

=item C<$content_type>

Content type of the error page.

=back

Note: C<$template> is a path relative to the L<core plugin|Plox::Plugin::Plox>'s C<$flavour_dir> option, the error
template should be put in this directory. Its file extension must differ from the usual flavour template extension
F<.flavour>!

If you use the L<XhtmlMime plugin|Plox::Plugin::XhtmlMime> plugin, the content type will be replaced if the client
prefers XHTML, so do not use other content types than C<'text/html'> in that case.

=head1 METHODS

The following hooks are implemented:

=head2 pre_process

L<General information for this hook.|Plox::Plugin/pre_process>

This hook will check if the requested template (or a default template) exists (only if enabled by the C<$check_flavour>
configuration option), it will check if stories were found for the requested page and if there are no invalid path
elements in the requested URL. If one of these checks fails, the template to be used will be set to the error template
file.

=cut

sub pre_process {
   my ($self, $data) = @_;
   my $error_404;
   $self -> {'404'} = 0;
   if ($self -> {'check_flavour'}) {
      my ($file, $path);
      $file = $data -> {'tt_settings'} {'file'} if $data -> {'tt_settings'} {'file'};
      $path = $data -> {'tt_settings'} {'path'} if $data -> {'tt_settings'} {'path'};
      $file ||= $data -> {'path'} {'flavour'} . '.flavour';
      $path ||= $data -> {'plugins'} {'Plox'} -> config_get ($data, 'flavour_dir');
      $error_404 = 1 unless -f "$path/$file" and -r _ or -f "$path/default.flavour" and -r _;
   }
   if (
      $error_404 or not ($data -> {'stories'} and %{$data -> {'stories'}}) or
      ($data -> {'path'} {'invalid'} and @{$data -> {'path'} {'invalid'}})
   ) {
      $data -> {'tt_settings'} {'file'} = $self -> {'template'};
      $self -> {'404'} = 1;
   }
   return Plox::Plugin::OK;
}

=head2 post_process

L<General information for this hook.|Plox::Plugin/post_process>

If the requested page caused a 404 error (see above), and no status header is set yet, it will be set to C<404>.
Additionally, the content type will be set. The content type will only be set if the C<Status> header is not set, any
existing content type setting will be overridden.

=cut

sub post_process {
   my ($self, $data) = @_;
   if ($self -> {'404'} and not $data -> {'headers'} {'Status'}) {
      $data -> {'headers'} {'Status'}       = '404';
      $data -> {'headers'} {'Content-type'} = $self -> {'content_type'};
   }
   return Plox::Plugin::OK;
}

1;

=head1 CHANGELOG

=over

=item 2011/12/20

=over

=item *

version 0.1.1

=item *

fixed a programming error that caused the 404 status to be set on existing pages in some cases

=back

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: