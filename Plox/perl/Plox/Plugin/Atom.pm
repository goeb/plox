# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::Atom;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1.1';

=head1 NAME

Plox::Plugin::Atom - Provides some Atom specific story data

=head1 DESCRIPTION

This plugin adds some stuff to the story data that is required to generate an Atom feed.

=head1 CONFIGURATION

=over

=item C<$flavour>

Flavour of the atom feeds. Used to determine the template.

=item C<$offset>

Your regular time zone offset, for example: C<'+01:00'>.

=item C<$offset_dst>

Offset for daylight saving time, for example: C<'+02:00'>.

=back

=head1 METHODS

The following hooks are implemented:

=head2 path_info

L<General information for this hook.|Plox::Plugin/path_info>

Unloads the plugin if the requested page flavour does not match the C<$flavour> configuration option. Make sure this
hook is run after the L<core plugin's path_info|Plox::Plugin::Plox/path_info> hook.

=cut

sub path_info {
   my ($self, $data) = @_;
   unless ($data -> {'path'} {'flavour'} eq $self -> {'flavour'}) {
      return Plox::Plugin::REFUSED | Plox::Plugin::UNLOAD;
   }
   return Plox::Plugin::OK;
}

=head2 story

L<General information for this hook.|Plox::Plugin/story>

This hook will modify and add some information to the story data. This is:

=over

=item C<< $story -> {'title'} >>

All HTML tags will be removed.

=item C<< $story -> {'Atom'} {'updated'} >>

Story time in a suitable format.

=item C<< $story -> {'Atom'} {'summary'} >>

First sentence of the story, without any tags.

=back

The variable C<< $data -> {'Atom'} {'updated'} >> is set to the C<< $story -> {'Atom'} {'updated'} >> value of the last
modified story (as long as the default sort order is retained).

This hook must be run when the story data is already available, i.e. after the
L<core plugin's story|Plox::Plugin::Plox/story> hook.

=cut

sub story {
   my ($self, $data, $entry, $story) = @_;
   return (Plox::Plugin::REFUSED, $story) unless $story;
   $story -> {'title'} =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;
   $story -> {'Atom'} {'updated'} =
      "$story->{date}[0]-$story->{date}[1]-$story->{date}[2]T" .
      "$story->{time}[0]:$story->{time}[1]:$story->{time}[2]";
   $story -> {'Atom'} {'updated'} .= $story -> {'time_info'} [2] ? $self -> {'offset_dst'} : $self -> {'offset'};
   $data -> {'Atom'} {'updated'} = $story -> {'Atom'} {'updated'} unless $data -> {'Atom'} {'updated'};
   ($story -> {'Atom'} {'summary'} = $story -> {'text'}) =~ s/(\.!?)\s.*$/$1/ms;
   $story -> {'Atom'} {'summary'} =~ s/<(?:[^>'"]*|(['"]).*?\1)*>//gs;
   $story -> {'Atom'} {'summary'} =~ s/\n/ /gms;
   return (Plox::Plugin::OK, $story);
}

1;

=head1 CHANGELOG

=over

=item 2009/06/08

=over

=item *

version 0.1.1

=item *

bug fixed: uninitialized or undefined C<$story> not treated correctly

=back

=item 2009/05/29

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: