# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::Today;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin::Today - Adds current date and time to the page data

=head1 DESCRIPTION

Provides some date and time specific data in C<$data> (all values are set to the time of the request):

=over

=item C<< $data -> {'Today'} {'second'} >>

Second, with leading zero if required.

=item C<< $data -> {'Today'} {'minute'} >>

Minute, with leading zero if required.

=item C<< $data -> {'Today'} {'hour'} >>

Hour, with leading zero if required.

=item C<< $data -> {'Today'} {'day_of_month'} >>

Day of month, with leading zero if required.

=item C<< $data -> {'Today'} {'month'} >>

Month, starts with 1 for January, with leading zero if required

=item C<< $data -> {'Today'} {'year'} >>

Year.

=item C<< $data -> {'Today'} {'day_of_week'} >>

Day of week, 0 is Sunday, 1 is Monday etc.

=item C<< $data -> {'Today'} {'day_of_year'} >>

Day of year, zero based.

=item C<< $data -> {'Today'} {'daylight_saving_time'} >>

Daylight saving time flag.

=item C<< $data -> {'Today'} {'day'} >>

Name of the day, as configured.

=item C<< $data -> {'Today'} {'month_name'} >>

Name of month, as configured.

=back

All this stuff is calculated when the I<pre_process> hook is executed.

=head1 CONFIGURATION

=over

=item C<@days>

An array(ref) containing the names of the days of a week. The array must start with the name of Sunday, so Perl's day of
week value as returned by the I<localtime()> function can be used as an index.

=item C<@months>

As above, only for month names, starts with January.

=back

=head1 METHODS

The following hooks are implemented:

=head2 pre_process

L<General information for this hook.|Plox::Plugin/pre_process>

Adds the information to the C<$data> hash(ref).

=cut

sub pre_process {
   my ($self, $data) = @_;
   my ($sec, $min, $hour, $d, $m, $y, $dow, $doy, $dst) = $self -> _local_time (time);
   $data -> {'Today'} = {
      'second'               => $sec,
      'minute'               => $min,
      'hour'                 => $hour,
      'day_of_month'         => $d,
      'month'                => $m,
      'year'                 => $y,
      'day_of_week'          => $dow,
      'day_of_year'          => $doy,
      'daylight_saving_time' => $dst,
      'day'                  => $self -> {'days'  } [$dow  ],
      'month_name'           => $self -> {'months'} [$m - 1],
   };
   return Plox::Plugin::OK;
}

1;

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: