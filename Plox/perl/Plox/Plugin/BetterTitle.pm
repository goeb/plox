# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::BetterTitle;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin::BetterTitle - Create a title based on the requested page

=head1 DESCRIPTION

The L<core plugin|Plox::Plugin::Plox> does not offer an option to create a title for the requested page based on its
content, this plugin does. The title will contain the categories for the requested page (if any), the date specification
(if any), and for story pages the title of the story.

The title will be based on a configured base title (the C<$title> config option), page specific information will be
appended. If the page is a date page, the C<$pre_date> config option is used as a prefix (but after the base title), for
category pages the C<$pre_category> option is used. For pages of type C<both> both will be included. Entries are
sparated by C<$separator>.

The generated title string will be saved in C<< $data -> {'BetterTitle'} >>.

=head1 CONFIGURATION

=over

=item C<$title>

General prefix for the title.

=item C<$separator>

Separator (must include spaces if required).

=item C<$pre_date>

Prefix for date stuff (must include trailing spaces if required).

=item C<$pre_category>

Prefix for category stuff (must include trailing spaces if required).

=back

=head1 METHODS

The following hooks are implemented:

=head2 pre_process

L<General information for this hook.|Plox::Plugin/pre_process>

Generates the title.

=cut

sub pre_process {
   my ($self, $data) = @_;
   $data -> {'BetterTitle'} = $self -> {'title'};
   if ($data -> {'path'} {'type'} eq 'story') {
      if (@{$data -> {'entries'} {'order'}}) {
         $data -> {'BetterTitle'} .= $self -> {'separator'} .
            $data -> {'stories'} {$data -> {'stories'} {'order'} [0] {'date'}} [0] {'title'};
      }
      return Plox::Plugin::OK;
   }
   if ($data -> {'path'} {'type'} eq 'date' or $data -> {'path'} {'type'} eq 'both') {
      my @date;
      foreach (@{$data -> {'path'} {'date'}}) {
         push @date, $_ if $_;
      }
      $data -> {'BetterTitle'} .= $self -> {'separator'} . $self -> {'pre_date'} . join '/', @date;
   }
   if ($data -> {'path'} {'type'} eq 'category' or $data -> {'path'} {'type'} eq 'both') {
      $data -> {'BetterTitle'} .= $self -> {'separator'} . $self -> {'pre_category'} .
         join ' / ', @{$data -> {'path'} {'categories'}};
   }
   return Plox::Plugin::OK;
}

1;

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: