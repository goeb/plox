# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::MoreEntries;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin::MoreEntries - Show previous/next page links on index pages

=head1 DESCRIPTION

Though the L<core plugin|Plox::Plugin::Plox> allows limiting the number of entries on index pages, it does not offer a
way to show the entries excluded by that limit. This plugin provides a page number feature, allowing previous page and
next page links on index pages.

The page numbers of the next and previous page will be available in the two variables:

=over

=item *

C<< $data -> {'MoreEntries'} {'next'} >>

=item *

C<< $data -> {'MoreEntries'} {'prev'} >>

=back

If there is no next or previous page, the appropriate entry will be set to C<0>.

B<Note:> With the default latest entries first order, next page (i.e. increasing page number) always means earlier
entries, and previous page (i.e. decreasing page number) means later entries, see the example template.

=head1 CONFIGURATION

There are no configuration options for this plugin, the C<$max_entries> option of the L<core plugin|Plox::Plugin::Plox>
limits the number of entries per page, see L<Plox::Plugin::Plox>.

=head1 METHODS

The following hooks are implemented:

=head2 new

L<General information for this hook.|Plox::Plugin/new>

The plugin will not be loaded if C<$max_entries> is not set.

=cut

sub new {
   my ($module, $data) = @_;
   return undef unless $data -> {'plugins'} {'Plox'} -> config_get ($data, 'max_entries');
   my $self = $module -> SUPER::new ($data);
   $self -> {'skipped'} = 0;
   $self -> {'skip'}    = 0;
   $self -> {'page'}    = 1;
   return bless $self, $module;
}

=head2 path_info

L<General information for this hook.|Plox::Plugin/path_info>

This hook will initialize the variables for the page stuff. The page number is retrieved from a category (since it is
handled like a regular category by the L<core plugin|Plox::Plugin::Plox>), the (pseudo-)category is removed from the
category related variables. It must be the last category, e.g.
L<http://example.org/2009/01/category/subcategory/page-2/index.html> is a valid URL with a page number. Note that the
only valid format is C<< 'page-<number>' >>.

=cut

sub path_info {
   my ($self, $data) = @_;
   return Plox::Plugin::REFUSED | Plox::Plugin::UNLOAD if $data -> {'path'} {'type'} eq 'story';
   if ($data -> {'path'} {'categories'} and @{$data -> {'path'} {'categories'}}) {
      my $last = $data -> {'path'} {'categories'} [$#{$data -> {'path'} {'categories'}}];
      if ($last =~ /^page-([1-9]\d{0,8})$/) {
         $self -> {'page'} = $1;
         pop @{$data -> {'path'} {'categories'}};
         if (not @{$data -> {'path'} {'categories'}}) {
            if ($data -> {'path'} {'type'} eq 'category') {
               $data -> {'path'} {'type'} = 'main';
            }
            elsif ($data -> {'path'} {'type'} eq 'both') {
               $data -> {'path'} {'type'} = 'date';
            }
         }
         $data -> {'path'} {'path'} = $self -> _join_path (
            @{$data -> {'path'} {'date'}}, @{$data -> {'path'} {'categories'}}
         );
         $data -> {'path'} {'category'} = $self -> _join_path (@{$data -> {'path'} {'categories'}});
      }
   }
   $self -> {'next'} = $self -> {'page'} + 1;
   $self -> {'prev'} = $self -> {'page'} - 1;
   $self -> {'skip'} = $self -> {'prev'} * $data -> {'plugins'} {'Plox'} -> config_get ($data, 'max_entries');
   return Plox::Plugin::OK;
}

=head2 filter_entry

L<General information for this hook.|Plox::Plugin/filter_entry>

Excludes an entry based on the page settings. Make sure to run this hook after all other I<filter_entry> hooks.

=cut

sub filter_entry {
   my ($self, $data, $entry, $keep, $filtered) = @_;
   return (Plox::Plugin::OK, 0) if not $keep;
   return (Plox::Plugin::OK, $self -> {'skipped'} ++ < $self -> {'skip'} ? 0 : 1);
}

=head2 pre_process

L<General information for this hook.|Plox::Plugin/pre_process>

Sets up the page numbers in C<$data>.

=cut

sub pre_process {
   my ($self, $data) = @_;
   $data -> {'MoreEntries'} {'next'} = $data -> {'entries'} {'more'} ? $self -> {'next'} : 0;
   $data -> {'MoreEntries'} {'prev'} = $self -> {'prev'};
   return Plox::Plugin::OK;
}

1;

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: