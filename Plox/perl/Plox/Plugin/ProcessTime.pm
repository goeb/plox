# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::ProcessTime;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

use Time::HiRes;

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin::ProcessTime - Measure the time it takes to generate a page

=head1 DESCRIPTION

This plugin calculates the time it takes to process a request and generate the page. Specifically, it measures three
different time values:

=over

=item *

The first time value available is the time it takes from loading the plugin to processing the template (without the
template processing itself). If you want to include this time on the output page, use it like every other variable in
the template, the value will be saved in C<< $data -> {'ProcessTime'} >>.

=item *

The second value is the time from loading the plugin until the output generation is (nearly) completed. Since at this
point the template has been processed already, you can't use variables in the template itself to include this time value
on a page. Instead, the first ocurrence of the string specified by the C<$replace_all> configuration option in the
generated page is replaced with the value.

=item *

The third value is the time it took to process the template, i.e. the difference between the former two values. Again,
inclusion in the page output is done by replacing the first occurrence of a string, specified by the C<$replace_tt>
configuration option.

=back

The format of the time value is specified by the C<$format> configuration option, it is a format string passed to the
I<sprintf()> function, see L<perlfunc> for details.

=head1 CONFIGURATION

=over

=item C<$format>

Format for the time values, passed to I<sprintf()>.

=item C<$replace_all>

Replace this string in the page output with the time it took to generate the page.

=item C<$replace_tt>

Replace this string in the page output with the time it took to process the template.

=back

If the C<$replace_all> and/or C<$replace_tt> options are set to false value (e.g. the empty string), no search is
performed for the specific string.

=head1 METHODS

The following hooks are implemented:

=head2 new

L<General information for this hook.|Plox::Plugin/new>

Saves the start time. Should be loaded as early as possible to provide a better result.

=cut

sub new {
   my ($module, $data) = @_;
   my $self = $module -> SUPER::new ($data);
   $self -> {'start_time'} = Time::HiRes::time;
   return bless $self, $module;
}

=head2 process

L<General information for this hook.|Plox::Plugin/process>

This hook provides the time elapsed since loading in the C<< $data -> {'ProcessTime'} >> variable, to be included in the
template.

It must be run before the L<core plugin's process|Plox::Plugin::Plox/process> hook.

=cut

sub process {
   my ($self, $data) = @_;
   $self -> {'process_time'} = Time::HiRes::time;
   $data -> {'ProcessTime'} = sprintf ($self -> {'format'}, $self -> {'process_time'} - $self -> {'start_time'});
   return Plox::Plugin::OK;
}

=head2 post_process

L<General information for this hook.|Plox::Plugin/post_process>

Replaces the two strings (see configuration options above) with the appropriate values.

=cut

sub post_process {
   my ($self, $data) = @_;
   my $time     = Time::HiRes::time;
   my $time_all = sprintf ($self -> {'format'}, $time - $self -> {'start_time'  });
   my $time_tt  = sprintf ($self -> {'format'}, $time - $self -> {'process_time'});
   $data -> {'output'} =~ s/\Q$self->{replace_all}\E/$time_all/ if $self -> {'replace_all'};
   $data -> {'output'} =~ s/\Q$self->{replace_tt}\E/$time_tt/ if $self -> {'replace_tt'};
   return Plox::Plugin::OK;
}

1;

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: