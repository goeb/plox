# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::Gallery;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

use File::Path;
use Image::Magick;
use Storable qw (lock_nstore lock_retrieve);

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin::Gallery - Create image galleries for blog posts

=head1 DESCRIPTION

This plugin can be used to add image galleries to blog posts by simply putting the images in a specific folder, the
image properties (e.g. file name, width and height, file size) will be available in the story data and can be used in
the templates.

The plugin will use the L<Image::Magick> module to get most of the image properties, the same module is used to create
thumbnails if required.

Images for a story will be searched in a subfolder of the C<$gallery_dir> directory. The folder must be named like the
blog entry, including category. For example, if you want to set up a gallery for the story saved as
F<category/subcategory/story.ext>, you need to put the images in the subdirectory F<category/subcategory/story/> in the
C<$gallery_dir> folder.

The images must be named F<< <base name>.<extension> >>, F<< <base name> >> must contain only characters
C<[a-zA-Z0-9_-]>. Generally, images are identified by their base name. An image with this name will be the main image
for a given base name, additional images for this base name must be named
F<< <base name>.<alternative>.<extension> >>, the rules mentioned above for the F<< <base name> >> part do apply to the
F<< <alternative> >> part, too. You may use this additonal images to include the same image in a different format,
different size, etc. A special case is the thumbnail image, with the file name F<< <base name>.thumb.<extension> >>
(with the extension set by the C<$thumbnail_extension> configuration option). There is only one thumbnail per base
name, obviously you can not create an alternative image with F<thumb> being the second part of the file name.

L<Image::Magick> is used to get the meta data of the images (width, height, etc.). Since this is an expensive operation,
especially for large images, the data will be stored in a subdirectory of the C<$cache_dir> directory (for the example
above, the cache file will be saved as F<category/subcategory/story> in C<$cache_dir>), unless the option
C<$prepare_gallery> is set to a false value. Galleries will be ignored unless there is a cache file, so if you set
C<$prepare_gallery> to a false value, you will have to create a cache file (and thumbnails if required) by yourself and
put it in the right place to set up a gallery for a story (use the F<Tools/create-gallery-cache.pl> script to create the
cache file). If the option is set to a true value, a cache file will be created automatically if a gallery directory for
a story exists (even if there are no images in that directory). Note that cache files will not be updated, so if you add
some images, you will have to delete the cache (or replace it with an updated file if you disabled C<$prepare_gallery>).

Thumbnails will be created automatically from the main image if they do not exist during the generation of the cache
file, subject to the C<$thumbnail_*> configuration options.

On index pages, stories with a gallery will only be included if the requested page's category is included in the
C<@categories> configuration option (i.e. if an element in this array matches the beginning of the requested category).
If the string C<'/'> is included, gallery posts will always be included. The maximum number of images per story on index
pages can be limited by the C<$max_on_index> option (C<-1> means no limit).

Descriptions for images can be included in the story file headers in the form C<< gallery: <image> <description> >>.
The descriptions will then be available in the template, see below.

=head1 CONFIGURATION

=over

=item C<$image_time>

Use the mtime of the latest image instead of the story file's mtime as post time.

=item C<@categories>

Only show gallery posts if a category in this array (or a subcategory of it) is requested.

=item C<$max_on_index>

Maximum number of images to include on index pages, set to C<-1> for no limit.

=item C<$gallery_dir>

Base directory of gallery images (no trailing slash).

=item C<@extensions>

Image extensions (including leading dot), other files will be ignored.

=item C<$cache_dir>

Base directory of the cache files (no trailing slash).

=item C<$thumbnail_extension>

Format of the thumbnails (used as extension, must be supported by L<Image::Magick>).

=item C<$thumbnail_width>

Width of the thumbnails to create.

=item C<$thumbnail_height>

Height of the thumbnails to create.

=item C<$prepare_gallery>

If true, cache file and thumbnails will be created if they don't exist.

=back

=head1 IMAGE DATA

The image stuff will be added to the story data. If a story has a gallery, C<< $story -> {'Gallery'} >> will be
available containing the following data to use in a template (eg. as C<[% story.Gallery.number %]>):

=over

=item C<$number>

Number of images found for the story. This does not include alternative versions of the same image.

=item C<@images>

An array of all images, the elements are hashrefs containing the following data:

=over

=item *

C<$name> - base name of the image

=item *

C<$thumbnail> - thumbnail file

=item *

C<$mtime> - mtime of the latest image (main image and any alternatives)

=item *

C<$alternatives> - number of alternatives for this image

=item *

C<@alt_images> - array containing the alternative images, sorted by alternative name

=item *

C<%main> - hash containing the meta data of the main image

=back

The C<@images> array is sorted by the C<$mtime> value, in descending order.

The C<@alt_images> array's elements are hashrefs, in the same format as the C<%main> hash(ref), with the following keys
(pretty self-explanatory):

=over

=item *

C<$filename>, C<$filesize>

=item *

C<$mtime>, C<$day>, C<$month>, C<$year>, C<$hour>, C<$minute>, C<$second>, C<$day_of_week>, C<$day_of_year>, C<$dst>

=item *

C<$compression>, C<$depth>, C<$format>, C<$height>, C<$interlace>, C<$quality>, C<$type>, C<$units>, C<$width>,
C<$x-resolution>, C<$y-resolution> (see the L<Image::Magick> documentation for these)

=item *

C<$alt_name> (alternative name - i.e. second part of the file name - set only for alternative images)

=back

All file names do not include any path, i.e. they are relative to the individual story's gallery directory.

=item C<%description>

A hash containing the descriptions for an image (if a description was specified in the story header). The hash's key is
the image file name, the value is the description. If there is no description for an image, the key will not exist in
the hash.

=back

=head1 REQUIREMENTS

The L<Image::Magick> module is required.

=head1 METHODS

The following hooks are implemented:

=head2 path_info

L<General information for this hook.|Plox::Plugin/path_info>

Decides if gallery posts should be shown for the requested page, based on the configuration. This hook must be run after
the L<core plugin's path_info|Plox::Plugin::Plox/path_info> hook.

=cut

sub path_info {
   my ($self, $data) = @_;
   $self -> {'show_posts'} = 0;
   $self -> {'categories'} = [ '/' ] unless @{$self -> {'categories'}};
   $self -> {'extension' } = $data -> {'plugins'} {'Plox'} -> config_get ($data, 'extension');
   foreach (@{$self -> {'categories'}}) {
      if ($data -> {'path'} {'category'} =~ /^\Q$_\E/) {
         $self -> {'show_posts'} = 1;
         last;
      }
   }
   return Plox::Plugin::OK;
}

=head2 entry

L<General information for this hook.|Plox::Plugin/entry>

Will set the entry's mtime to the mtime of the lastest image (if any), based on the configuration. If there is a gallery
for a story, the data will be saved for later use. If a category was requested for which no gallery posts should be
shown, this hook will return C<(Plox::Plugin::NEXT_ENTRY, undef)>, causing all remaining plugins to be skipped and
discarding the entry (only on index pages, single story pages will work as expected). To make sure this works, this hook
must be run before other I<entry> hooks.

=cut

sub entry {
   my ($self, $data, $file, $entry) = @_;
   my ($img_number, $mtime, $images) = $self -> _get_images ($data, $file);
   if ($img_number) {
      if (not $self -> {'show_posts'} and $data -> {'path'} {'type'} ne 'story') {
         return (Plox::Plugin::NEXT_ENTRY, undef);
      }
      my $base = $self -> _entry_base ($data, $file);
      if ($data -> {'path'} {'type'} ne 'story') {
         if ($self -> {'max_on_index'} == 0) {
            $images = [];
         }
         elsif ($self -> {'max_on_index'} != -1 and $self -> {'max_on_index'} < scalar @{$images}) {
            @{$images} = @{$images} [0 .. $self -> {'max_on_index'} - 1];
         }
      }
      $self -> {'images'} {$base . $self -> {'extension'}} = {
         'number' => $img_number,
         'images' => $images,
      };
      return (Plox::Plugin::OK, {'mtime' => $mtime}) if $self -> {'image_time'};
      return (Plox::Plugin::OK, $entry);
   }
   return (Plox::Plugin::REFUSED, $entry);
}

=head2 story

L<General information for this hook.|Plox::Plugin/story>

Adds the image stuff to the story data, simply copying the stuff saved in the I<entry> hook to the C<$story> hash, and
adding the image descriptions from the story headers. The headers must be available, i.e. this hook must be run after
the L<core plugin's story|Plox::Plugin::Plox/story> hook.

=cut

sub story {
   my ($self, $data, $entry, $story) = @_;
   return (Plox::Plugin::REFUSED, $story) unless $story and $self -> {'images'} {$entry};
   $story -> {'Gallery'} {'number'} = $self -> {'images'} {$entry} {'number'};
   $story -> {'Gallery'} {'images'} = $self -> {'images'} {$entry} {'images'};
   if ($story -> {'headers'} {'gallery'}) {
      my @descriptions = split /\n/, $story -> {'headers'} {'gallery'};
      foreach (@descriptions) {
         if (/^(.+?)\s+(.+)$/) {
            $story -> {'Gallery'} {'description'} {$1} = $2;
         }
      }
   }
   return (Plox::Plugin::OK, $story);
}

=head1 PRIVATE METHODS

=head2 _entry_base

First parameter is C<$data>, second parameter is a file name of a story file. The function returns the base name, i.e.
the name relative to the data directory and without file extension.

=cut

sub _entry_base {
   my ($self, $data, $file) = @_;
   my $ext = $self -> {'extension'};
   $file =~ s/^\Q$data->{entries}{base}\E//;
   $file =~ s/\Q$ext\E$//;
   return $file;
}

=head2 _get_images

Returns the image data as required by the L<entry|/entry> hook. Data is loaded from the cache file if it exists by the
L<Storable> module's I<lock_retrieve()> function. If no cache exists, a cache file is created (data is stored using
the L<Storable>'s I<lock_nstore()> function).

=cut

sub _get_images {
   my ($self, $data, $file) = @_;
   (my $cache = $self -> _join_path ($self -> {'cache_dir'}, $self -> _entry_base ($data, $file))) =~ s{/*$}{};
   if (-f $cache) {
      my $img = lock_retrieve ($cache);
      return $img ? @$img : undef;
   }
   elsif (not $self -> {'prepare_gallery'}) {
      return;
   }
   my $img_dir = $self -> _join_path ($self -> {'gallery_dir'}, $self -> _entry_base ($data, $file));
   return unless chdir $img_dir;
   my %images;
   my $thumb_ext = $self -> {'thumbnail_extension'};
   while (my $img = glob '*') {
      next unless -f $img and -r _;
      next if $img !~ /^([a-zA-Z0-9_-]+)(\.[a-zA-Z0-9_-]+)?\.[a-zA-Z0-9_-]+$/ or ($2 and $2 eq '.thumb');
      my $basename    = $1;
      my $alternative = $2;
      my $is_img;
      foreach (@{$self -> {'extensions'}}) {
         if ($img =~ /\Q$_\E$/) {
            $is_img = 1;
            last;
         }
      }
      next unless $is_img;
      my $mtime    = (stat _) [9];
      my $filesize = -s _;
      my $magick   = Image::Magick -> new ();
      my $err      = $magick -> Read ($img);
      if ($err) {
         warn "Image::Magick error '$err' for file $img\n";
         next;
      }
      $images {$basename} ||= {
         'name'         => $basename,
         'thumbnail'    => "$basename.thumb.$thumb_ext",
         'mtime'        => $mtime,
         'alternatives' => 0,
         'alt_images'   => [],
      };
      $images {$basename} -> {'mtime'} = $mtime if $mtime > $images {$basename} -> {'mtime'};
      my ($sec, $min, $hour, $d, $m, $y, $dow, $doy, $dst) = $self -> _local_time ($mtime);
      my %data = (
         'mtime'       => $mtime,
         'filename'    => $img,
         'filesize'    => $filesize,
         'day'         => $d,
         'month'       => $m,
         'year'        => $y,
         'hour'        => $hour,
         'minute'      => $min,
         'second'      => $sec,
         'day_of_week' => $dow,
         'day_of_year' => $doy,
         'dst'         => $dst,
      );
      my @get_data = qw (
         compression depth format height interlace quality type units width x-resolution y-resolution
      );
      $data {$_} = $magick -> Get ($_) foreach (@get_data);
      if ($alternative) {
         ++ $images {$basename} -> {'alternatives'};
         ($data {'alt_name'} = $alternative) =~ s/^\.//;
         push @{$images {$basename} -> {'alt_images'}}, \%data;
      }
      else {
         $images {$basename} -> {'main'} = \%data;
         unless (-f "$img_dir/$basename.thumb.$thumb_ext") {
            $self -> _create_thumb ($magick, "$img_dir/$basename.thumb.$thumb_ext");
         }
      }
   }
   my $sort_img = sub {
      my $x = $images {$a} -> {'mtime'};
      my $y = $images {$b} -> {'mtime'};
      return $x == $y ? lc ($a) cmp lc ($b) : $y <=> $x;
   };
   my $sort_alt = sub {
      return lc ($a -> {'alt_name'}) cmp lc ($b -> {'alt_name'});
   };
   my @images;
   foreach my $basename (sort $sort_img keys %images) {
      next unless $images {$basename} -> {'main'};
      @{$images {$basename} -> {'alt_images'}} = sort $sort_alt @{$images {$basename} -> {'alt_images'}};
      push @images, $images {$basename};
   }
   my $mtime = @images ? $images [0] -> {'mtime'} : 0;
   my @store = (scalar @images, $mtime, \@images);
   (my $cache_dir = $cache) =~ s{[^/]+$}{};
   mkpath ($cache_dir) unless -d $cache_dir;
   lock_nstore (\@store, $cache);
   return @store;
}

=head2 _create_thumb

Creates a thumbnail. First parameter must be an L<Image::Magick> instance containing the original image, the second
parameter must be the full path to the thumbnail file to write. The file's extension will set the image format to use.

=cut

sub _create_thumb {
   my ($self, $magick, $file) = @_;
   $magick -> Scale ('width' => $self -> {'thumbnail_width'}, 'height' => $self -> {'thumbnail_height'});
   $magick -> Write ($file);
}

1;

=head1 CHANGELOG

=over

=item 2009/06/08

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: