# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin::Paragraphs;

use warnings;
use strict;
use utf8;

use base 'Plox::Plugin';

our $VERSION = '0.1.1';

=head1 NAME

Plox::Plugin::Paragraphs - Automatically insert <p> tags

=head1 DESCRIPTION

This plugin modifies the story text to contain <p> tags. Every paragraph in the story will be enclosed by the tags,
paragraphs must be separated by an empty line in the story file for this to work. Without empty lines in the story, the
complete story text will be enclosed in the <p> tags.

There is a global configuration option C<$class>, if this is set, it will be used to set the value of a class attribute
for the <p> tag. Additionally, to override the setting on a per-story basis, you may set the story header
C<paragraph-class>. If you set the story header C<no-paragraphs> to some true value, this plugin will not modify the
story at all.

=head1 CONFIGURATION

=over

=item C<$class>

If set, global setting for the class attribute of the <p> tags.

=back

=head1 METHODS

The following hooks are implemented:

=head2 story

L<General information for this hook.|Plox::Plugin/story>

Inserts the paragraphs as described above. Make sure the story text is already available when this hook is run.

=cut

sub story {
   my ($self, $data, $entry, $story) = @_;
   return (Plox::Plugin::REFUSED, $story) if not $story or $story -> {'headers'} {'no-paragraphs'};
   my $class = $story -> {'headers'} {'paragraph-class'}
      ? $story -> {'headers'} {'paragraph-class'}
      : $self -> {'class'};
   $class = $class ? qq( class="$class") : '';
   $story -> {'text'} = "<p$class>$story->{text}</p>";
   $story -> {'text'} =~ s{\n\n}{</p>\n\n<p$class>}g;
   return (Plox::Plugin::OK, $story);
}

1;

=head1 CHANGELOG

=over

=item 2009/06/08

=over

=item *

version 0.1.1

=item *

bug fixed: uninitialized or undefined C<$story> not treated correctly

=back

=item 2009/05/29

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: