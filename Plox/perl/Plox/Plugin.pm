# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Plugin;

use warnings;
use strict;
use utf8;

our $VERSION = '0.1';

=head1 NAME

Plox::Plugin - Base class for Plox plugins

=head1 DESCRIPTION

This module includes some useful functions for I<Plox> plugin modules, and should be used by every plugin as the base
class.

=head1 PLUGINS

Most of I<Plox>' functionality is provided by plugins, with the core plugin L<Plox::Plugin::Plox> doing most of the
work. When a new instance of L<Plox> is created, after some basic initialization, all plugins are loaded (unless
disabled in the L<config file|Plox::Config>). When the L<generate()|Plox/generate> method of the L<Plox> instance is called, it will
simply run a number of methods the plugins may provide - from now on refered to as hooks. Every plugin may implement one
or more of these hooks and it will be run by the L<generate()|Plox/generate> method, the plugin may alter the gathered
page data to provide features otherwise not available.

=head1 HOOKS

Hook methods are run in the order specified below, if all plugins implementing a specific hook are finished, the next
hook is run. The order in which plugins are executed for a specific hook can be configured on a per-hook basis in the
configuration file, see L<Plox::Config> for details.

All regular hooks will be called with one argument, the C<$data> hashref, that contains all the data gathered by the
various plugins, as well as configuration, all plugin instances, and some more stuff. The exact structure of this hash
is L<described below|/THE $data HASH> and in the plugin modules that add or change the content.

The regular hooks must return one scalar value, it is interpreted as a bitmask using the L<constants|/CONSTANTS>
described below.

In addition to the regular hooks there are a few special hooks, which are called by the
L<core plugin|Plox::Plugin::Plox> from within other (regular) hooks. These special hooks take other arguments and must
return other values. They are described below at the place they are run.

The currently defined hooks are described below, they are called in the specified order. The information below covers
just the basics, take a look at the L<Plox::Plugin::Plox> documentation for implementation details, especially if you
need to know under which key some information is saved in C<$data>.

=head2 new

Actually, this is the constructor of the plugin. It is executed when the plugin is loaded, i.e. during the
L<new()|Plox/new> method of L<Plox> (all other hooks are run when the L<generate()|Plox/generate> method is called). It
takes C<$data> as one and only argument - just like the regular hooks - but its return value must be the plugin instance
- or C<undef>, if the plugin for some reason does not want to (or can not) be loaded. The order in which plugins are
loaded can be modified using C<'new'> as hook name in the C<%hooks_first> and/or C<%hooks_last>
L<configuration options|Plox::Config/General_Options>.

The plugin must perform all required initialization tasks in this constructor. A L<new()|/new (constructor)> method is
implemented in I<Plox::Plugin>, if a plugin does not need any more initialization as provided by this, it may not need
to implement it by itself (assuming it uses this class as base class). See L<below|/new (constructor)> for a description
of the L<new()|/new (constructor)> method provided by this module.

=head2 path_info

The purpose of this hook is to extract all required information - like requested category or story - from the requested
URL and put it into the C<$data> hashref for later use.

=head2 entries_root

This hook must set up the information about the base directory in which the story files are located.

=head2 entries

All files (i.e. stories) that should be considered for further processing are collected by this hook. This should
actually include B<all> story files beneath the base directory (set up by the L<previous hook|/entries_root>), subject
to configured restrictions (e.g. the maximum directory depth on index pages) to save some file system accesses. The
files will be L<sorted later on|/sort>, and the final decision if a file will be included on the requested page will be
made L<after that|/filter>. To avoid unnecessary directory accesses from plugins that also need to get a list of files
from this directory, plugins may implement two special hooks if they want to affect the file selection. The core plugin
will call the following special hooks when it runs L<its entries hook|Plox::Plugin::Plox/entries> for every file found:

=head3 accept_file

This hook takes two arguments, first the usual C<$data> hashref, the second one is the complete path of a file
found beneath the base directory.

It must return one value, interpreted as bitmask unsing the constants below:

=over

=item *

If the C<YES> bit is set, it indicates that the file should be considered for further processing. By default a file is
not included unless one hook's return value has this bit set. Processing stops for a given file when C<YES> is returned
by a plugin's I<accept_file> hook, remaining plugins will be skipped.

=item *

The only other bit that may be set in the return value of this hook is C<NEXT_ENTRY>. If this is set, the file will be
included if C<YES> is also set. Else it will not be included, but all remaining plugins will be skipped and processing
continues with the next file.

=item *

Other bits are silently ignored. The hook should return C<OK> (i.e. no bit set) if it does not want the file to be
included and leave the decision to remaining plugins.

=back

=head3 entry

This hook must provide the data which is used for sorting the files later on, in most cases, this is just the
modification time of the story file.

The hook is called with three arguments. The first one is the C<$data> hashref. The second one is the full path to the
story file. The third argument is a hashref, which is undef for the first I<entry> hook executed, and contains the
results of previous plugins' I<entry> hooks for all subsequent plugins.

The hook must return a list containing two values. The first is interpreted as a bitmask, if the C<NEXT_ENTRY> bit is
set, the current results for the file (as returned by the second element in the list) are final, processing continues
with the next file. The second element must be a hashref, containing the required data for later processing of the file,
especially for sorting all stories. This is the same hashref that is in turn passed to the next plugin's I<entry> hook
as third argument, so a hook may decide to handle a file only if it has not been taken care of yet, or even to modify
the results of previous plugins. If this second element is C<undef> or an empty hashref after the last I<entry> hook,
the file will be discarded from further processing. For details on the actual structure required by the core plugin see
L<Plox::Plugin::Plox>.

This two special hooks are most likely all you need to implement in a plugin used to include/exclude files other than
the default.

B<Note:> If only a single story is requested, the core plugin will only look for that specific file (i.e. the requested
base name with the configured extension) and run only the I<entry> hook for this file! No other files are considered in
this case and no C<accept_file> hook is run!

=head2 sort

This hook is used to sort the story entries gathered by the previous hooks. It must set up an array in C<$data>
containing the desired story order, see L<Plox::Plugin::Plox|Plox::Plugin::Plox/sort> for implementation details.

=head2 filter

After the L<entries|/entries> hook set up a list of all potential stories for the requested page and the L<sort|/sort>
hook took care of ordering, this hook may be used to filter out unwanted stories. This is most useful for limiting the
number of stories per page, or to include only stories matching a requested date.

The main plugin will call the special hook L<filter_entry|/filter_entry> for every entry included by previous hooks:

=head3 filter_entry

This hook will be called with four arguments: C<$data>, C<$entry>, C<$keep>, C<$filtered> (in that order).

=over

=item C<$data>

The first argument is the usual C<$data> hashref.

=item C<$entry>

C<$entry> will be the base name of the entry to check, i.e. the path to the story file relative to the C<$data_dir>
directory (configured in the L<main plugin's configuration|Plox::Plugin::Plox/CONFIGURATION>) including the file
extension.

=item C<$keep>

C<$keep> will be the decision of previous plugins' I<filter_entry> hooks whether to keep the entry or not, it will be
true for the first I<filter_entry> hook called for an entry.

=item C<$filtered>

C<$filtered> will be an arrayref, containing all entries that already passed the filters and are included in the page,
the format of the entries is the same as for the C<$entry> parameter.

=back

The hook must return a list with two values. The first one is interpreted as a bitmask, if the C<NEXT_ENTRY> bit is set,
the decision of the current hook will be final, other plugins' I<filter_entry> hooks will be skipped for the entry and
processing continues with the next entry. Other bits are currently ignored, return C<OK> if processing should continue
as normal. The second value in this list must be true if the hook decides that the entry should be kept, or false if the
entry should be excluded. This value is passed to the next plugin's I<filter_entry> hook in the C<$keep> parameter. The
decision to keep an entry or to discard it is based on the return values of the last I<filter_entry> hook executed for
the entry.

If an entry was included by the I<filter_entry> hook, it may still be rejected by the I<filter> hook itself, the main
plugin for example will restrict the number of entries per page after all I<filter_entry> hooks have been executed for
an entry. Also, this hook will not be executed for remaining entries once the maximum number has been reached!

=head2 stories

This hook is used to get the actual story data (i.e. text, title, headers etc.) from the entries. The
L<core plugin|Plox::Plugin::Plox> will call the special hook L<story|/story> for every entry, if there is a valid story
returned after the last special hook for an entry is finished the story is included. See the
L<core plugin's documentation|Plox::Plugin::Plox> for details.

=head3 story

This is a special hook, run by the I<stories> hook of the L<core plugin|Plox::Plugin::Plox> for every entry. This hook
should be used to provide the data for the specified story, i.e. the story's title, text, headers etc.

The hook is called with three parameters: The C<$data> hashref is the first one. The second one is the current entry,
i.e. the base name of the entry to get the story for (the same C<$entry> as used by previous special hooks). The third
one is a hashref containing the story data gathered by previous I<story> hooks, it will be C<undef> for the first hook
called for an entry.

The hook must return a list with two values. The first one is interpreted as a bitmask: If the C<NEXT_ENTRY> bit is set,
the story data of the current hook will be used, other plugins' I<story> hooks will be skipped for the entry and
processing continues with the next entry. Other bits are currently ignored, return C<OK> if processing should continue
as normal. The second value in this list must be a hashref with the story data (this is the same hashref used as third
argument for subsequent I<story> hooks for the entry).

The main plugin requires some specific data to be set for a story, see L<Plox::Plugin::Plox> for details.

=head2 pre_process

After all stories have been processed, this hook may be used to perform some final tasks before the gathered data is
fed to L<Template Toolkit|Template> to create the page output. This hook is not implemented by the
L<core plugin|Plox::Plugin::Plox>.

=head2 process

This hook's purpose is to create the actual page requested by the client. The L<core plugin|Plox::Plugin::Plox> uses
L<Template Toolkit|Template> and all the C<$data> to do this. There is usually no need for other plugins to implement
this hook.

=head2 post_process

If a plugin wants to perform any task after the page content was created, it may use this hook. The
L<core plugin|Plox::Plugin::Plox> does not implement it.

=head1 CONSTANTS

The following constants are defined, and must be used as return values by all regular hooks, they may be combined with
bitwise or (C<|>) if required.

=over

=item C<OK>

Indicates that everything is ok.

=item C<ERROR>

There was an error processing the hook function.

=item C<REFUSED>

The hook did not want to process as normal for some reason.

=item C<NEXT_HOOK>

Skip all remaining plugins for the current hook and continue with the next hook.

=item C<LAST>

Stop processing entirely, no further hooks are executed.

=item C<NO_OUTPUT>

Indicates that the main script must not produce any output.

=item C<UNLOAD>

Use this if a plugin should be unloaded.

=item C<NEXT_ENTRY>

Only used in special hooks, see above.

=item C<YES>

Only used in special hooks, see above.

=back

=head1 THE $data HASH

The C<$data> hash(ref) is used by all plugins to communicate and store the page data, it will be the argument for all
regular hooks. Most stuff in C<$data> is provided by the L<core plugin|Plox::Plugin::Plox>, see L<Plox::Plugin::Plox>
for details. The following variables are stored in C<$data> by the L<Plox> instance itself:

=over

=item C<< $data -> {'template'} >>

Template instance set by L<Plox> if provided to the L<new()|Plox/new> function.

=item C<< $data -> {'cgi'} >>

L<CGI> instance.

=item C<< $data -> {'cache'} >>

Cached data, if provided to the L<new()|Plox/new> function.

=item C<< $data -> {'conf'} >>

Contains the configuration hashref as returned by L<Plox::Config::config()|Plox::Config/config>.

=item C<< $data -> {'hooks'} {<hook>} >>

Array of plugins providing the hook C<< <hook> >>, in correct order as configured.

=item C<< $data -> {'plugins'} >>

Hash of plugin instances, plugin names are the keys.

=back

The L<Plox> module expects the following data in C<$data> after all plugins/hooks are done:

=over

=item C<< $data -> {'template'} >>

L<Template> instance used.

=item C<< $data -> {'cgi'} >>

L<CGI> instance used.

=item C<< $data -> {'headers'} >>

Headers to be sent to the client (including status header if required).

=item C<< $data -> {'output'} >>

Generated page content.

=item C<< $data -> {'cache'} >>

Data to be cached during requests, only used by plugins.

=back

=cut

use constant {
   'OK'           =>   0,
   'ERROR'        =>   1,
   'REFUSED'      =>   2,
   'NEXT_HOOK'    =>   4,
   'LAST'         =>   8,
   'NO_OUTPUT'    =>  16,
   'UNLOAD'       =>  32,
   'NEXT_ENTRY'   =>  64,
   'YES'          => 128,
};

=head1 METHODS

The following methods are implemented in the L<Plox::Plugin> base class:

=head2 new (constructor)

The new function provided by this base class will make all configuration options in C<< $config -> {<plugin>} >>
available for the plugin as C<< $self -> {<option name>} >> and set C<< $self -> {'NAME'} >> to the plugin's base name.

=cut

sub new {
   my ($module, $data) = @_;
   (my $plugin = $module) =~ s/.*:://;
   my $self = $data -> {'conf'} {$plugin};
   $self -> {'NAME'} = $plugin;
   return bless $self, $module;
}

=head2 _join_path

The argument list is joined with C<'/'> as separator and the resulting string is returned. Unlike the regular I<join()>
function, this one ensures that the path starts and ends with a C<'/'>, several consecutive slashes are replaced by a
single one. If the argument list is empty (or contains only empty elements) the string returned will be a single slash.

=cut

sub _join_path {
   my ($self, @paths) = @_;
   (my $path = '/' . join ('/', @paths) . '/') =~ s{/+}{/}g;
   return $path;
}

=head2 _local_time

Works nearly exactly as Perl's I<localtime()> function, but converts the values of the seconds, minutes, hours, days and
months into two-digit strings with leading zeroes if necessary. Additionally, the month value will be one-based instead
of the zero-based value I<localtime()> returns, and the year will be the actual year (i.e. the value returned by
I<localtime()> plus 1900).

=cut

sub _local_time {
   my ($self, $time) = @_;
   my ($s, $m, $h, $d, $mon, $y, $dow, $doy, $dst) = localtime ($time);
   ($s, $m, $h, $d, $mon) = map { sprintf ('%02d', $_) } ($s, $m, $h, $d, $mon + 1);
   return ($s, $m, $h, $d, $mon, $y + 1900, $dow, $doy, $dst);
}

=head2 _categories

Returns an arrayref containing the categories for the entry specified as argument to that function and their relative
paths, the first element will always be C<< { 'path' => '/' , 'name' => 'main' } >> for the top level category, the
remaining elements will be the parent categories for the given entry. For subsequent entries, the category's name will
be the value for C<'name'>, and the name is appended to the previous C<'path'> value.

=cut

sub _categories {
   my ($self, $entry) = @_;
   my @categories     = ({'path' => '/', 'name' => 'main'});
   my @paths          = split /\//, $entry;
   my $dir            = '/';
   foreach (@paths [0 .. $#paths - 1]) {
      push @categories, {'path' => $dir .= "$_/", 'name' => $_};
   }
   return \@categories;
}

=head2 config_set

Changes a configuration option of the plugin. Parameters are C<$data> (the usual data hashref), C<$setting> (the name of
the configuration option) and C<$value> (the new value of this option), in that order. This method will only work if the
setting to be changed is set in the configuration hashref, it will return 1 on success, a false value in case of an
error.

=cut

sub config_set {
   my ($self, $data, $setting, $value) = @_;
   if (exists $data -> {'conf'} {$self -> {'NAME'}} {$setting}) {
      $self -> {$setting} = $value;
      $data -> {'conf'} {$self -> {'NAME'}} {$setting} = $value;
      return 1;
   }
   return;
}

=head2 config_get

Returns a configuration option of the plugin. Parameters are C<$data> (the usual data hashref) and C<$setting> (the name
of the configuration option), in that order. This method will only work if the setting is set in the configuration
hashref, it will return the setting's current value on success, a false value in case of an error (note that the setting
itself may also be a false value).

=cut

sub config_get {
   my ($self, $data, $setting) = @_;
   if (exists $data -> {'conf'} {$self -> {'NAME'}} {$setting}) {
      return $self -> {$setting};
   }
   return;
}

1;

=head1 CHANGELOG

=over

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: