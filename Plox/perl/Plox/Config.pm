# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Config;

use warnings;
use strict;
use utf8;

our $VERSION = '0.1';

=head1 NAME

Plox::Config - Provides the configuration for Plox

=head1 SYNOPSIS

   my $config = Plox::Config -> config ();

=head1 DESCRIPTION

At the moment, this module acts as a configuration file for I<Plox> and all the plugins. It defines the main config
hashref and the L<config()|/config> function, that returns this hashref. This function is called by L<Plox> when a new
L<Plox> instance is created, it must not be called by the main application directly.

=head1 CONFIGURATION OPTIONS

B<Important:> Do not delete empty arrays or hashrefs in the configuration, even if you don't use them!

B<Note:> In the documentation all options are refered to as variables, for example C<%plugins_skip>, which actually
means the hashref C<< $config -> {'plugins_skip'} >>. It is easier to write and read, and has the advantage of including
the type (though everything in C<$config> is a reference, except the actual scalar values).

The configuration below specifies all currently used options, including the options for the plugins. For details on the
plugins' configuration options, see the documentation of the plugin they belong to, they are not described in this file.

=cut

my $config;

=head2 General Options

These options specify the order in which plugins should be loaded and L<hooks|Plox::Plugin/HOOKS> are run. Used by the
L<Plox> module itself.

=over

=item C<%plugins_skip>

This hashref must contain the names of plugins as keys and a true value assigned to it if a plugin should not be loaded
at all.

=cut

$config -> {'plugins_skip'} = {
   'Gallery' => 1,
};

=item C<%hooks_first>

This hashref's keys are names of L<hooks|Plox::Plugin/HOOKS> (special hooks and the L<new()|Plox::Plugin/new> method may
be included, too). The values of the hashref must be arrayrefs, containing plugin names, in the order the plugins should
be run for that specific hook. Plugins included here will be run before all other plugins for that hook. If the name of
the hook is C<'new'>, it refers to the order in which the plugin instances should be created.

=cut

$config -> {'hooks_first'} = {
   'new'       => [ 'ProcessTime', 'Plox'                      ],
   'path_info' => [ 'Plox', 'Gallery'                          ],
   'entry'     => [ 'Gallery, ''Plox'                          ],
   'story'     => [ 'Plox', 'SeyMour', 'Paragraphs', 'Gallery' ],
   'process'   => [ 'ProcessTime'                              ],
};

=item C<%hooks_last>

The opposite of the C<%hooks_first> option. The syntax is the same as above. Plugins specified here will be run after
all other plugins for the specific hook. The last plugin in the arrayref will be run as the last plugin for the hook. If
a plugin is specified in both C<%hooks_first> and C<%hooks_last> options, its appearance in C<%hooks_last> will be
ignored. Plugins not included will be run between the first and last plugins, in alphabetical order.

=cut

$config -> {'hooks_last'} = {
   'path_info'    => [ 'Atom', 'MoreEntries'                  ],
   'filter_entry' => [ 'MoreEntries'                          ],
   'story'        => [ 'Atom'                                 ],
   'pre_process'  => [ 'NotFound'                             ],
   'post_process' => [ 'NotFound', 'XhtmlMime', 'ProcessTime' ],
};

=back

The configuration examples provided for C<%hooks_first> and C<%hooks_last> specify the correct order if all plugins are
enabled. If you do not require a specific plugin, it is recommended to not change this setting, so you can easily enable
it again. Just include the plugin in the C<%plugins_skip> option above so it will not be loaded.

=head2 Plugin Specific Options

All configuration options for the plugins must be put into a separate hashref in C<$config>, with the plugin name as the
key. This ensures that no plugin accidentally overrides another plugin's settings. As mentioned above, settings specific
to the plugins are documented in the plugin modules.

=over

=item C<%Atom>

See L<Plox::Plugin::Atom>.

=cut

$config -> {'Atom'} = {
   'flavour'    => 'atom',
   'offset'     => '+01:00',
   'offset_dst' => '+02:00',
};

=item C<%BetterTitle>

See L<Plox::Plugin::BetterTitle>.

=cut

$config -> {'BetterTitle'} = {
   'title'        => 'Plox',
   'separator'    => ' ➔ ',
   'pre_date'     => 'archive: ',
   'pre_category' => 'category: ',
};

=item C<%Gallery>

See L<Plox::Plugin::Gallery>.

=cut

$config -> {'Gallery'} = {
   'image_time'          => 1,
   'categories'          => [ '/' ],
   'max_on_index'        => 2,
   'gallery_dir'         => '/path/to/your/gallery',
   'extensions'          => [ '.tif', '.png', '.jpg', '.jpeg', '.gif' ],
   'cache_dir'           => '/path/to/your/cache',
   'thumbnail_extension' => 'png',
   'thumbnail_width'     => 100,
   'thumbnail_height'    => 100,
   'prepare_gallery'     => 0,
};

=item C<%NotFound>

See L<Plox::Plugin::NotFound>.

=cut

$config -> {'NotFound'} = {
   'check_flavour' => 1,
   'template'      => 'error.404',
   'content_type'  => 'text/html; charset=utf8',
};

=item C<%Paragraphs>

See L<Plox::Plugin::Paragraphs>.

=cut

$config -> {'Paragraphs'} = {
   'class' => '',
};

=item C<%Plox>

See L<Plox::Plugin::Plox>.

=cut

$config -> {'Plox'} = {
   'default_page'    => 'index',
   'default_flavour' => 'html',
   'data_dir'        => '/path/to/your/story/files',
   'extension'       => '.blog',
   'max_depth'       => -1,
   'future_entries'  => 0,
   'max_entries'     => 5,
   'flavour_dir'     => '/path/to/your/flavours',
   'compile_ext'     => undef,
   'compile_dir'     => undef,
   'types'           => {
      'html'         => 'text/html; charset=utf-8',
      'atom'         => 'application/xml; charset=utf-8',
   },
};

=item C<%ProcessTime>

See L<Plox::Plugin::ProcessTime>.

=cut

$config -> {'ProcessTime'} = {
   'format'      => '%.05f',
   'replace_all' => '<!--ProcessTimeAll-->',
   'replace_tt'  => '<!--ProcessTimeTT-->',
};

=item C<%SeyMour>

See L<Plox::Plugin::SeyMour>.

=cut

$config -> {'SeyMour'} = {
   'cut' => '<!--SeyMour-->',
};

=item C<%Today>

See L<Plox::Plugin::Today>.

=cut

$config -> {'Today'} = {
   'days'   => [ qw( Sunday Monday Tuesday Wednesday Thursday Friday Saturday                              ) ],
   'months' => [ qw( January February March April May June July August September October November December ) ],
};

=item C<%XhtmlMime>

See L<Plox::Plugin::XhtmlMime>.

=cut

$config -> {'XhtmlMime'} = {
   'flavours' => [ 'html' ],
   'type'     => 'application/xhtml+xml; charset=utf-8',
   '404'      => 1,
};

=back

=head1 METHODS

=head2 config

Returns the configuration hashref.

=cut

sub config { return $config; }

1;

=head1 INTERNALS

Having the configuration options directly set in the module is probably not the best way to do this, on the other hand,
it is simple and flexible. However, since there is only this one function which must return the configuration, there is
still the possibility to change this in future versions (though I doubt I will ever change it).

Plugins will have full access to the configuration, even to the configuration of other plugins. However, access to other
plugins' settings must be done using the provided functions, see L<Plox::Plugin> for details.

Plugins may change the configuration options only if there is no other way to achieve the desired behaviour. If a plugin
modifies the configuration, it must document the changes and note every possible incompatibility with other plugins.
Again, the way to change settings of other plugins is to use the provided methods of L<Plox::Plugin>.

=head1 CHANGELOG

=over

=item 2009/06/07

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: