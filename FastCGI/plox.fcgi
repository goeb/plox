#!/usr/bin/perl

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

use warnings;
use strict;
use utf8;

=head1 NAME

plox.fcgi - Plox main CGI script - FastCGI version

=head1 DESCRIPTION

This is the main CGI script of Plox. This script is executed by the server if requested, it creates a L<Plox> instance
and runs its L<generate()|Plox/generate> method to generate the requested page's content, which is then printed to the
standard output and so sent to the client.

=over

B<Note:> This is the FastCGI version of the script. Make sure the L<CGI::Fast> module is available and your server
supports FastCGI if you want to use it! A regular CGI script called F<plox.cgi> is also available. If your server meets
the requirements, it is recommended to use the FastCGI version for better performance.

=back

For more general information on L<Plox>, see the F<README> file included in this distribution.

=head1 CONFIGURATION

You need to set the

   use lib '/path/to/modules/';

directive to the path of the I<Plox::*> modules (unless they can be found in a path already in C<@INC>, then you may
want to delete this line).

=cut

use lib '/path/to/modules/';

use CGI::Fast;
use Plox;

=pod

Set the C<$check_script> variable to a number greater than zero to enable the script to check itself for modification on
disk every C<$check_script> accesses and exit if it has been modified, it should be reloaded by the server automatically
in this case. This check is disabled if C<$check_script> is set to C<0>, in that case you may have to kill the process
manually if you change the script.

=over

B<Note:> This will not check the imported modules, if one of these changes, you may have to kill the process manually in
order to load the new version! But you may change the mtime of the script (eg. with the L<touch(1)> command) without
changing the content to reload it. This applies to a changed configuration, too!

=back

See L<Plox::Config> for details on the configuration of L<Plox> itself.

=cut

my $check_script = 10;

=head1 DETAILS

The script will wait for incoming connections in the main loop until the server kills it or it detects that it has been
modified on disc (by simply checking the modification time of the file) if enabled.

=over

B<Note:> Check of the modification time is done before anything else, but if the file has been modified, the script will
exit only after the current request has been processed (i.e. after a page has been sent to the client).

=back

The L<Plox> instance is created, some stuff it requires will be cached between accesses, most notably this will be the
case for the template objects, which should improve the performance significantly. The L<Plox> instance itself can not
be cached since it is specific for every request! If there is an error, a 500 server error will be sent to the client.

=over

B<Note:> If creating the L<Plox> instance fails for 10 times consecutively, the script will exit with error code 1! On
the first successful access the error counter will be reset. If the object creation fails and a script modification has
been detected, it will exit with error code 1 regardless of the number of errors.

=back

The message sent to the client in case of an error is not configurable at the moment, if you want to send a custom
message, modify the code directly at the appropriate place in F<plox.fcgi>. In addition to the error message sent to the
client, an error message is sent to STDERR (via C<warn()>), this is usually logged in the server's error log file.

If everything went fine, the page headers and page output will be printed, unless L<Plox> told the script otherwise.

=head1 SIGNAL HANDLING

When the script receives a C<SIGPIPE>, it will ask the L<Plox> instance to stop processing the current request, see
L<Plox> for details and limitations on this. The handlers for C<SIGTERM> and C<SIGUSR1> are currently disabled, they do
not work as expected with Perl's safe signal handling (introduced in version 5.7.3). The handlers for these two signals
should make sure that the current request can finish and the script exits afterwards (or exit immediately if no request
is processed when the signal is received). However, the handlers won't be run when waiting for a new request, but
immediately after a new request is accepted, causing the script to exit without any output sent to the server, this
eventually causes a 500 server error. See the FastCGI documentation for further information on how it is supposed to be
done, especially L<http://www.fastcgi.com/docs/faq.html#Signals>.

=cut

$SIG  {'PIPE'} = \&sigpipe;
#$SIG {'USR1'} = \&sigterm;
#$SIG {'TERM'} = \&sigterm;

my $requests   = 0;
my $errors     = 0;
my $exit       = 0;
my $processing = 0;
my $plox;
my $template;
my $cache;

while (my $cgi = new CGI::Fast ()) {

   $processing = 1;

   if (++ $requests == $check_script) {
      $exit = 1 if -M $0 < 0;
      $requests = 0;
   }

   $plox = new Plox (
      'cgi'       => $cgi,
      'template'  => $template,
      'cache'     => $cache,
   );

   binmode STDOUT, ':encoding(UTF-8)';
   use if $FCGI::VERSION > 0.68, 'bytes';

   if (not $plox) {

      warn "Plox: Could not create the main Plox instance\n";
      (my $error = <<'      END') =~ s/^\s{9}//gm;
         Status: 500 Internal Server Error
         Content-type: text/plain; charset=utf-8

         Internal Server Error ― It is not your fault… Or is it?

         The error was:

            ⚠  Could not create the main instance

         This may be a temporary problem, please try again later.
      END
      print $error;
      exit 1 if ++ $errors == 10;

   }
   else {

      $errors = 0;

      my ($result, $data) = $plox -> generate ();

      unless ($result & Plox::NO_OUTPUT) {
         if ($data -> {'headers'}) {
            my $headers = 0;
            while (my ($header, $value) = each %{$data -> {'headers'}}) {
               if ($value) {
                  print $header, ': ', $value, "\n";
                  $headers = 1;
               }
            }
            print "\n" if $headers;
         }
         print $data -> {'output'} if $data -> {'output'};
      }

      $template = $data -> {'template'};
      $cache    = $data -> {'cache'};

      undef $plox;

   }

   $processing = 0;

   exit if $exit;

}

sub sigpipe {
   $plox -> terminate () if $plox;
}

sub sigterm {
   exit unless $processing;
   $exit = 1;
}

=head1 CHANGELOG

=over

=item 2011/12/10

=over

=item *

version 0.1.1

=item *

bugfix/workaround for FCGI.pm > 0.68

=back

=item 2009/05/31

=over

=item *

version 0.1

=item *

initial release

=back

=back

=head1 LICENSE

Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>

This file is part of Plox.

Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with Plox. If not, see
<L<http://www.gnu.org/licenses/>>.

=cut

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: