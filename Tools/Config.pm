# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

package Plox::Config;

use warnings;
use strict;
use utf8;

our $VERSION = '0.1';

my $config;

# This is a more end user friendly version of the Configuration file of Plox. Basically, all you need to change are the
# path settings $data_dir and $flavour_dir below to get Plox running.
#
# For details on the plugins see the appropriate plugin modules, documentation is available in POD format. Use perldoc
# to display it, eg. «perldoc Atom.pm».

# Configuration of the Plox core plugin:
$config -> {'Plox'} = {

   # The directory where your story files are located. Do not use a trailing slash!
   'data_dir'        => '/path/to/your/story/files',

   # The file name extension of your story files, including the leading dot:
   'extension'       => '.blog',

   # Set this to the directory where your flavour templates are located, without a trailing slash!
   'flavour_dir'     => '/path/to/your/flavours',

   # If a page with this name is requested (or no specific page at all), an index page is generated:
   'default_page'    => 'index',

   # If the user does not request a specific flavour, use this one:
   'default_flavour' => 'html',

   # This option specifies how much subcategory levels are included on index pages. Set to 0 to only include stories in
   # in the requested category, 1 to include subcategories one level below the requested category etc. Set $max_depth to
   # -1 to not restrict the subcategory depth.
   'max_depth'       => -1,

   # Set this to 1 to display stories with a modification time in the future, 0 to not show them:
   'future_entries'  => 0,

   # $max_entries specifies how many stories will be displayed on index pages, a value of 0 means unlimited:
   'max_entries'     => 5,

   # These two options are actually Template Toolkit options, set these if you want Template Toolkit to save compiled
   # templates to disk, in the directory specified by $compile_dir, and with the extension $compile_ext. You need to set
   # both options if you want to use this feature! See the Template Toolkit documentation for details.
   'compile_ext'     => undef,
   'compile_dir'     => undef,

   # You need to set the content types for every flavour, in «'flavour-name' => 'content-type'» format:
   'types'           => {
      'html'         => 'text/html; charset=utf-8',
      'atom'         => 'application/xml; charset=utf-8',
   },

};

# Configuration of the XhtlMime plugin:
$config -> {'XhtmlMime'} = {

   # Change the content type for the flavours in @flavours:
   'flavours' => [ 'html' ],

   # Change the content type to this value:
   'type'     => 'application/xhtml+xml; charset=utf-8',

   # Check the content type for 404 error pages, too. Requires the NotFound plugin.
   '404'      => 1,

};

# Configuration of the SeyMour plugin:
$config -> {'SeyMour'} = {

   # On index pages, everything after (and including) the string specified by $cut will be deleted from a story:
   'cut' => '<!--SeyMour-->',

};

# Configuration of the ProcessTime plugin:
$config -> {'ProcessTime'} = {

   # The format of the time values, must be a format string as used by sprintf:
   'format'      => '%.05f',

   # This string will be replaced with the complete time it took to generate a page:
   'replace_all' => '<!--ProcessTimeAll-->',

   # This string will be replaced with the template processing time:
   'replace_tt'  => '<!--ProcessTimeTT-->',

};

# Configuration of the Paragraphs plugin:
$config -> {'Paragraphs'} = {

   # The default class to use for the inserted <p> tags, set to an empty string to not set a class:
   'class' => '',

};

# Configuration of the BetterTitle plugin:
$config -> {'BetterTitle'} = {

   # Base title of your blog:
   'title'        => 'Plox',

   # Separate different title parts with this string:
   'separator'    => ' ➔ ',

   # Prepend this string to date parts of the title:
   'pre_date'     => 'archive: ',

   # Prepend this string to category parts of the title:
   'pre_category' => 'category: ',

};

# Configuration of the Atom plugin:
$config -> {'Atom'} = {

   # The Atom feed flavour, the plugin will do nothing unless this flavour is requested:
   'flavour'    => 'atom',

   # Timezone offset to UTC of your blog:
   'offset'     => '+01:00',

   # Timezone offset to UTC of your blog, when daylight saving time is effective:
   'offset_dst' => '+02:00',

};

# Configuration of the NotFound plugin:
$config -> {'NotFound'} = {

   # Check if a template for the requested flavour (or a default flavour) exists:
   'check_flavour' => 1,

   # Template to use for 404 error pages, relative to the $flavour_dir setting of the Plox plugin:
   'template'      => 'error.404',

   # Content type to be used for 404 error pages:
   'content_type'  => 'text/html; charset=utf8',

};

# Configuration of the Today plugin:
$config -> {'Today'} = {

   # Names of the days, starting with Sunday:
   'days'   => [ qw( Sunday Monday Tuesday Wednesday Thursday Friday Saturday ) ],

   # Names of the months, starting with January:
   'months' => [ qw( January February March April May June July August September October November December ) ],

};

# Configuration of the (EXPERIMENTAL!) Gallery plugin:
$config -> {'Gallery'} = {

   # Use modification time of latest image instead of story file time:
   'image_time'          => 1,

   # Gallery posts will be shown for categories in this array, with leading and trailing slashes:
   'categories'          => [ '/' ],

   # Include this number of images per story on index pages, -1 = unlimited:
   'max_on_index'        => 2,

   # Base directory of gallery images, no trailing slash:
   'gallery_dir'         => '/path/to/your/gallery',

   # Image extensions (including leading dot), all other files will be ignored:
   'extensions'          => [ '.tif', '.png', '.jpg', '.jpeg', '.gif' ],

   # Base directory of the cache files, no trailing slash:
   'cache_dir'           => '/path/to/your/cache',

   # Thumbnail format (will be used as file extension, no leading dot, must be supported by ImageMagick):
   'thumbnail_extension' => 'png',

   # Thumbnail width and height:
   'thumbnail_width'     => 100,
   'thumbnail_height'    => 100,

   # If no cache file is found, check if a gallery exists, generate cache (and thumbnails if required):
   'prepare_gallery'     => 0,

};

# %plugins_skip:
#
# To disable a plugin, set the value %plugins_skip to 1, for example, use «'Atom' => 1,» to disable the Atom plugin.

$config -> {'plugins_skip'} = {
   'Atom'        => 0,
   'BetterTitle' => 0,
   'Gallery'     => 1,
   'MoreEntries' => 0,
   'NotFound'    => 0,
   'Paragraphs'  => 0,
   'ProcessTime' => 0,
   'SeyMour'     => 0,
   'Today'       => 0,
   'XhtmlMime'   => 0,
};

# %hooks_first and %hooks_last:
#
# There is usually no need to change anything in %hooks_first and %hooks_last. See the development version of this
# file for a description.

$config -> {'hooks_first'} = {
   'new'       => [ 'ProcessTime', 'Plox'                      ],
   'path_info' => [ 'Plox', 'Gallery'                          ],
   'entry'     => [ 'Gallery', 'Plox'                          ],
   'story'     => [ 'Plox', 'SeyMour', 'Paragraphs', 'Gallery' ],
   'process'   => [ 'ProcessTime'                              ],
};

$config -> {'hooks_last'} = {
   'path_info'    => [ 'Atom', 'MoreEntries'                  ],
   'filter_entry' => [ 'MoreEntries'                          ],
   'story'        => [ 'Atom'                                 ],
   'pre_process'  => [ 'NotFound'                             ],
   'post_process' => [ 'NotFound', 'XhtmlMime', 'ProcessTime' ],
};

# DO NOT CHANGE ANYTHING BELOW!

sub config {
   return $config;
}

1;

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: