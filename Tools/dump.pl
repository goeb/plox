#!/usr/bin/perl

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# Dump data stored in a file created by Storable.
#
# Usage: dump.pl <file>

use warnings;
use strict;

use Data::Dumper;
use Storable qw (lock_retrieve);

my $data = lock_retrieve ($ARGV [0]);
print Dumper ($data);

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: