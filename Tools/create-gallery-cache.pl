#!/usr/bin/perl

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# Creates a cache file for the Gallery plugin.
#
# Usage:
#
#  create-gallery-cache.pl <image dir> <output file> [<thumb width> <thumb height> <thumb format> <image extensions>]
#
#  <image dir>          gallery directory for the story
#  <output file>        file to save the cache
#  <thumb width>        thumbnail width (default: 100)
#  <thumb height>       thumbnail height (default: 100)
#  <thumb format>       thumbnail format, used as file extension (default: png)
#  <image extensions>   extensions of image files (default: .png .jpg .jpeg .tif .gif)
#
# Example:
#
#  create-gallery-cache.pl ./images/ ./cache 100 100 png .png .jpg

use warnings;
use strict;
use utf8;

use Cwd;
use File::Path;
use Image::Magick;
use Storable qw (lock_nstore lock_retrieve);

my ($dir, $out, $tw, $th, $tf, @extensions) = @ARGV;

die "No image directory or directory does not exist\n" unless $dir and -d $dir;
die "No output file or file exists\n"                  unless $out and not -e $out;

$tw ||= 100;
$th ||= 100;
$tf ||= 'png';

@extensions = qw (.png .jpg .jpeg .tif .gif) unless @extensions;

my $old_dir = cwd ();
die "Could not cd to $dir\n" unless chdir $dir;

my %images;

while (my $img = glob '*') {
   next unless -f $img and -r _;
   next if $img !~ /^([a-zA-Z0-9_-]+)(\.[a-zA-Z0-9_-]+)?\.[a-zA-Z0-9_-]+$/ or ($2 and $2 eq '.thumb');
   my $basename    = $1;
   my $alternative = $2;
   my $is_img;
   foreach (@extensions) {
      if ($img =~ /\Q$_\E$/) {
         $is_img = 1;
         last;
      }
   }
   next unless $is_img;
   my $mtime    = (stat _) [9];
   my $filesize = -s _;
   my $magick   = Image::Magick -> new ();
   my $err      = $magick -> Read ($img);
   if ($err) {
      warn "Image::Magick error '$err' for file $img\n";
      next;
   }
   $images {$basename} ||= {
      'name'         => $basename,
      'thumbnail'    => "$basename.thumb.$tf",
      'mtime'        => $mtime,
      'alternatives' => 0,
      'alt_images'   => [],
   };
   $images {$basename} -> {'mtime'} = $mtime if $mtime > $images {$basename} -> {'mtime'};
   my ($sec, $min, $hour, $d, $m, $y, $dow, $doy, $dst) = _local_time ($mtime);
   my %data = (
      'mtime'       => $mtime,
      'filename'    => $img,
      'filesize'    => $filesize,
      'day'         => $d,
      'month'       => $m,
      'year'        => $y,
      'hour'        => $hour,
      'minute'      => $min,
      'second'      => $sec,
      'day_of_week' => $dow,
      'day_of_year' => $doy,
      'dst'         => $dst,
   );
   my @get_data = qw( compression depth format height interlace quality type units width x-resolution y-resolution );
   $data {$_} = $magick -> Get ($_) foreach (@get_data);
   if ($alternative) {
      ++ $images {$basename} -> {'alternatives'};
      ($data {'alt_name'} = $alternative) =~ s/^\.//;
      push @{$images {$basename} -> {'alt_images'}}, \%data;
   }
   else {
      $images {$basename} -> {'main'} = \%data;
      unless (-f "$dir/$basename.thumb.$tf") {
         _create_thumb ($magick, "$dir/$basename.thumb.$tf");
      }
   }
}

die "Could not cd to $old_dir\n" unless chdir $old_dir;

my $sort_img = sub {
   my $x = $images {$a} -> {'mtime'};
   my $y = $images {$b} -> {'mtime'};
   return $x == $y ? lc ($a) cmp lc ($b) : $y <=> $x;
};

my $sort_alt = sub {
   return lc ($a -> {'alt_name'}) cmp lc ($b -> {'alt_name'});
};

my @images;
foreach my $basename (sort $sort_img keys %images) {
   next unless $images {$basename} -> {'main'};
   @{$images {$basename} -> {'alt_images'}} = sort $sort_alt @{$images {$basename} -> {'alt_images'}};
   push @images, $images {$basename};
}

my $mtime = @images ? $images [0] -> {'mtime'} : 0;
my @store = (scalar @images, $mtime, \@images);
lock_nstore (\@store, $out);

exit 0;

sub _local_time {
   my ($time) = @_;
   my ($s, $m, $h, $d, $mon, $y, $dow, $doy, $dst) = localtime ($time);
   ($s, $m, $h, $d, $mon) = map { sprintf ('%02d', $_) } ($s, $m, $h, $d, $mon + 1);
   return ($s, $m, $h, $d, $mon, $y + 1900, $dow, $doy, $dst);
}

sub _create_thumb {
   my ($magick, $file) = @_;
   $magick -> Scale ('width' => $tw, 'height' => $th);
   $magick -> Write ($file);
}

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: