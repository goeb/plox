#!/bin/bash

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# Restores the modification time of files saved with the save-mtime.sh script. Set the DIR and DB variables below to the
# data directory and the file that contains the modification times. The script prints a content type header, so it can
# be used as a CGI script.
#
# IMPORTANT: This script does not work for dates before September 9th, 2001, 01:46:40 UTC!

# Data directory:
DIR=/path/to/your/data/directory

# Modification time database file:
DB=mtimes

echo "Content-type: text/plain\n\n";
echo

while read LINE ; do

   LINE=${LINE##Content-type: text/plain*}
   if [[ -n "$LINE" ]] ; then

      TIME=${LINE:0:11}
      FILE=${LINE##$TIME }

      echo "setting time $TIME for $FILE"

      touch -m -d "$TIME" "$DIR/$FILE"

   fi

done <"$DB"

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=120: