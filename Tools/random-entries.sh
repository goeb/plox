#!/bin/bash

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# Run this in the data directory. It will create NUM blog entries, each in a different category (actually the entries
# are created in ./<category>/<subcategory1>/<subcategory2>/.../<subcategoryX>/, the depth is specified by the
# SUBCATEGORIES variable below). The stories will contain random strings, the output of the pwgen command. The mtime of
# the stories will be set to a somewhat random value.
#
# Requirements: The bc and pwgen commands are required.

# DEPTH specifies the depth of the stories, set to 1 to create stories in the top level category (ie. the current
# directory), set to 2 to create a category for each story, 3 to create a category and a subcategory etc. Do not set it
# to 0!
DEPTH=4

# Extension of story files. Must match that one set in the Plox config file.
EXT=".blog"

# Number of stories to create.
NUM=1000

for i in `seq 1 $NUM` ; do

   FILE=`pwgen -CN$DEPTH | sed 's/ /\//g'`$EXT

   if [[ $DEPTH -gt 0 ]]; then
      DIR=${FILE##*/}
      mkdir -p "$DIR"
   fi

   pwgen -CN48 >$FILE

   TIME=$( echo "`date +%s` - $RANDOM * 1000 + $RANDOM * 100 - $RANDOM * 10 + $RANDOM" | bc )
   touch -m -d @"$TIME" "$FILE"

done

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=120: