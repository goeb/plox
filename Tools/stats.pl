#!/usr/bin/perl

# Copyright 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# Simple statistics script.
#
# usage: stats.pl <url> <requests>
#
# Requests <url> <requests> times, url must send the example html flavour. The ProcessTime plugin must be enabled. The
# three different time values are extracted from the page and statistics are shown at the end. Results of each requests
# are printed to STDERR, so if you just want the average/min/max values redirect STDERR to /dev/null.
#
# There is no error checking done!

use warnings;
use strict;

die "usage: $0 <url> <requests>\n" unless $ARGV [0] and $ARGV [1];

use LWP::UserAgent;

my $ua = LWP::UserAgent -> new ();
$ua -> agent ("PloxStats/0.1");

my $req    = HTTP::Request -> new (GET => $ARGV [0]);
my $errors = 0;
my $cnt    = 0;
my $step1  = { 'max' => 0, 'min' => 0, 'all' => 0, };
my $step2  = { 'max' => 0, 'min' => 0, 'all' => 0, };
my $all    = { 'max' => 0, 'min' => 0, 'all' => 0, };

for (my $i = 1; $i <= $ARGV [1]; $i ++) {

   my $res = $ua -> request ($req);

   if ($res -> is_success ()) {
      my $c = $res -> content ();
      if ($c =~ m{starts: (\d+\.\d+)s<br />.*processing: (\d+\.\d+)s<br />.*generation: (\d+\.\d+)s}mis) {
         ++ $cnt;
         $step1 -> {'max'} = $1 if $1 > $step1 -> {'max'};
         $step1 -> {'min'} = $1 if not $step1 -> {'min'} or $1 < $step1 -> {'min'};
         $step1 -> {'all'} += $1;
         $step2 -> {'max'} = $2 if $2 > $step2 -> {'max'};
         $step2 -> {'min'} = $2 if not $step2 -> {'min'} or $2 < $step2 -> {'min'};
         $step2 -> {'all'} += $2;
         $all -> {'max'} = $3 if $3 > $all -> {'max'};
         $all -> {'min'} = $3 if not $all -> {'min'} or $3 < $all -> {'min'};
         $all -> {'all'} += $3;
         print STDERR $1, '  ', $2, '  ', $3, "\n";
      }
   }
   else {
      ++ $errors;
   }

}

die "no successful requests\n" unless $cnt;

print "\nstep1    step2    all\n";
printf 
   "%.05f  %.05f  %.05f  < average values\n",
   $step1 -> {'all'} / $cnt,
   $step2 -> {'all'} / $cnt,
   $all   -> {'all'} / $cnt
;
printf "%.05f  %.05f  %.05f  < minimum values\n", $step1 -> {'min'}, $step2 -> {'min'}, $all -> {'min'};
printf "%.05f  %.05f  %.05f  < maximum values\n", $step1 -> {'max'}, $step2 -> {'max'}, $all -> {'max'};

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: