#!/usr/bin/env perl

# Copyright 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.

use strict;
use warnings;

use File::Path qw( remove_tree );
use Pod::ProjectDocs;

my $destination = $ARGV [0];
die "Usage: pod2html <output directory>" unless -d $destination;

remove_tree ($destination, { 'verbose' => 1, 'keep_root' => 1, 'safe' => 1 });

Pod::ProjectDocs -> new (
   'outroot'   => $destination,
   'libroot'   => [qw( Plox/perl CGI FastCGI )],
   'except'    => [qr!^XXX$!],
   'title'     => 'Plox',
   'desc'      => 'A simple Perl blogging application.',
   'index'     => 1,
   'lang'      => 'en',
   'forcegen'  => 1,
) -> gen ();

__END__

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: