#!/bin/sh

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# CGI script to kill the plox.fcgi process. May require modifications if your output of the ps command differs!

echo "Content-type: text/plain"
echo
echo "Your running processes:"
echo
ps
echo

for PID in `ps | grep ' plox.fcgi' | sed 's/^ *//' | cut -d' ' -f1` ; do
   echo "Killing $PID"
   kill $PID
done

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=120: