#!/usr/bin/perl

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# Adds an UTF-8 BOM to the files specified as command line arguments. Usually not required.
# Do not use for very large files!

use strict;
use warnings;

die "no files specified\n" unless @ARGV;

my $bom = "\x{EF}\x{BB}\x{BF}";

foreach my $file (@ARGV) {
   next unless -f $file and -r _ and -w _;
   print 'checking ', $file, '... ';
   local $/;
   open (FILE, '<:bytes', $file) or die "can't open $file for reading: $!\n";
   my $content = <FILE>;
   close FILE or die "can't close $file: $!\n";
   if (substr ($content, 0, 3) ne $bom) {
      open (FILE, '>:bytes', $file) or die "can't open $file for writing: $!\n";
      print FILE $bom, $content;
      close FILE or die "can't close $file: $!\n";
      print "modified\n";
   }
   else {
      print "already ok\n";
   }
}

# :indentSize=3:tabSize=3:noTabs=true:mode=perl:maxLineLen=120: