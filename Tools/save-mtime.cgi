#!/bin/bash

# Copyright 2009-2011, 2014 Stefan Goebel <plox -at- subtype -dot- de>
#
# This file is part of Plox.
#
# Plox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# Plox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Plox. If not, see
# <http://www.gnu.org/licenses/>.
#
########################################################################################################################
#
# Print the modification times of files in the $DIR directory to STDOUT, redirect output to some file to save the
# mtimes. The file can later be used with the restore-mtime.sh script to restore the modification times of the files.
# This script will print a content type header, so it can be used as a cgi script without modification. The content type
# header may be included in the database file, restore-mtime.sh will simply ignore it. Note: files included are not
# restricted to story files!

# Data directory:
DIR=/path/to/your/data/directory

echo "Content-type: text/plain\n\n"
echo

cd "$DIR"

find . -type f | xargs stat -c "@%Y %n"

cd - >/dev/null

# :indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=120: